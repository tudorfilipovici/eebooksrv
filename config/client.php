<?php

/**
 * Client configuration
 * Default values are stored in .env file
 */
return [
  'partner_code' => env('CLIENT_PARTNER_CODE', '2ES'),
  'lang'         => env('CLIENT_LANG',         'en'),
  'pos'          => env('CLIENT_POS',          'DE'),
  'ip_address'   => env('CLIENT_IP_ADDRESS',   '127.0.0.1'),
  'user_id'      => env('CLIENT_USER_ID',      ''),
  'module_id'    => env('CLIENT_MODULE',       'fb'),
  'session_id'   => env('CLIENT_SESSION_ID',   '123N0S3S10N4567'),

  // 'booking_servers' => ['mldtest'],
  'booking_servers' => ['lcxtest'],
  'booking_ports'   => [2500],

  'trace' => true, // logg all mw messages
];
