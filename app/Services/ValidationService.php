<?php
namespace App\Services;

use \DateTime;
use App\Utils\DateTimeUtil;
use App\Models\Route;
use Illuminate\Validation\Validator;
use Illuminate\Support\Arr;

/**
 * Contains custom validators for our app
 * Prefix all the services with 'twoe' to make obvious they are custom.
 */
class ValidationService extends Validator
{
  /**
   * Validates the value for the selected field is less or equal to the given one
   * Usage:
   * 
   *     twoe_less_equal_field:fieldName
   * 
   * @param  string   $attribute  The attribute name
   * @param  mixed    $value      The value
   * @param  array    $parameters Array with the name of the field to compare
   * @return boolean  True if validation passes
   */
  function validateTwoeLessEqualField($attribute, $value, $parameters=[]) {
    $this->requireParameterCount(1, $parameters, 'twoe_less_equal_field');
    $other = Arr::get($this->data, $parameters[0]);
    return $value <= $other;
  }

  /**
   * Validates the value for the selected field is less or equal to the given one
   * Usage:
   * 
   *     twoe_route_exists
   * 
   * @param  string   $attribute  The attribute name
   * @param  array    $value      The value must be an array with 2 strings [originCode, destinCode]
   * @param  array    $parameters Not used
   * @return boolean  True if validation passes
   */
  function validateTwoeRouteExists($attribute, $values, $parameters=[]) {
    $lang   = config('client.lang');
    $origin = reset($values);
    $destin = next($values);

    if (!$origin || !$destin) {
      return false;
    }

    $result = Route::allPartnerRoutes($lang, $origin, $destin);
    return !$result->isEmpty();
  }

  /**
   * Usage:
   * 
   *     twoe_departure:returnField
   * 
   * @param  string   $attribute  The attribute name
   * @param  mixed    $value      The value
   * @param  array    $parameters Array with the name of the field to compare
   * @return boolean  True if validation passes
   */
  function validateTwoeDeparture($attribute, $value, $parameters=[]) {
    $field     = reset($parameters); 
    $departure = DateTime::createFromFormat(DATE_FORMAT_DE, $value);
    $return    = false;

    if (Arr::has($this->data, $field)) {
      $return = DateTime::createFromFormat(DATE_FORMAT_DE, Arr::get($this->data, $field)); // returns false on error
    }
    
    $result = DateTimeUtil::isValidDeparture($departure, $return ?: null);
    return $result === true;
  }

  /**
   * Usage:
   * 
   *     twoe_return:departureField
   * 
   * @param  string   $attribute  The attribute name
   * @param  mixed    $value      The value
   * @param  array    $parameters Array with the name of the field to compare
   * @return boolean  True if validation passes
   */
  function validateTwoeReturn($attribute, $value, $parameters=[]) {
    $this->requireParameterCount(1, $parameters, 'twoe_return');

    $field = reset($parameters);

    if (!Arr::has($this->data, $field)) {
      return false;
    }

    $return    = DateTime::createFromFormat(DATE_FORMAT_DE, $value);
    $departure = DateTime::createFromFormat(DATE_FORMAT_DE, Arr::get($this->data, $field));

    $result = DateTimeUtil::isValidReturn($return, $departure);
    return $result === true;
  }
}