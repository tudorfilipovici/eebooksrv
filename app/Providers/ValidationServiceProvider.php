<?php
namespace App\Providers;

use App\Services\ValidationService;
use Illuminate\Support\ServiceProvider;
use Validator as ValidatorFacade;

/**
 * Custom service provider to reconfigure
 * See: http://stackoverflow.com/questions/28417977/custom-validator-in-laravel-5
 */
class ValidationServiceProvider extends ServiceProvider {

  public function boot()
  {
    // Modify the facade to return a different validator
    ValidatorFacade::resolver(function($translator, $data, $rules, $messages)
    {
      return new ValidationService($translator, $data, $rules, $messages);
    });
  }

  public function register()
  {
    // This is an abstract method that needs to be defined
  }
}