<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use App\Models\ScreenText;

class TextsController extends Controller
{
  /**
   * Retrieves screen texts from the DB
   * @param  Request $request The request data
   * @return array            Hash map with the requested scripts
   */
  public function retrieve(Request $request) {
    // $this->validate($request, [
    //   'functions' => 'required'
    // ]);

    $scripts = $request->input('functions', '');
    if (!is_array($scripts)) {
      $scripts = explode(',', $scripts);
    }

    $lang    = config('client.lang');
    $results = [
      'languages' => ScreenText::languages(),
      'texts'     => []
    ];
    foreach ($scripts as $script) {
      if ($script) {
        $results['texts'][$script] = $this->getScriptTexts($lang, $script);
      }
    }
    return $results;
  }

  /**
   * Retrieves all the screen texts for a function and language
   * @param  string $lang   Language
   * @param  string $script Function id
   * @return array          Hash with key/values for screen texts
   */
  protected function getScriptTexts($lang, $script) {    
    $result = ScreenText::getArray($lang, $script);
    if (!isset($result[$lang][$script])) {
      return [];
    }
    return $result[$lang][$script];
  }
}