<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\PartnerConfig;

/**
 * This middleware will handle app configuration settings like the language and client
 */
class ConfigMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    // Look for the 'lang' language parameter
    if (isset($request->lang)) {
      // Check is a valid language code
      $languages = explode(',', PartnerConfig::get('languages'));
      if (in_array($request->lang, $languages)) {
        config(['client.lang' => $request->lang]);
      }
    }

    return $next($request);
  }
}
