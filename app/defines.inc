<?php
define('MLD_AIRPORT', 'KIV');

// Date and DateTime Formats
define('DATE_NOW',                 'now');
define('DATE_FORMAT_DE',           'd.m.Y');
define('DATE_FORMAT_ISO',          'Y-m-d');
define('DATE_FORMAT_RO',           'd/m/Y');
define('DATE_FORMAT_SHORT',        'd M, Y');
define('DATETIME_FORMAT_DE',       'd.m.Y H:i:s');
define('DATETIME_FORMAT_ISO',      'Y-m-d H:i:s');
define('DATETIME_FORMAT_RO',       'd/m/Y H:i:s');
define('DATETIME_FORMAT_INTERNAL', 'd.m.Y Hi');
define('DATETIME_FORMAT_SHORT',    'M d, Y H:i');
define('DATETIME_FORMAT_ISO8601',  'Y-m-d\TH:i:sP');

// Defaults for date inputs
define('DATE_DEPARTURE_DEFAULT', 'P1D');
define('DATE_RETURN_DEFAULT',    'P8D');
define('DATE_MIN_DEPARTURE',     'PT6H'); // 6 hrs from now
define('DATE_MIN_RETURN',        'PT2H'); // 2 hrs from departure
define('DATE_SALE_OPEN_UNTIL',   '2017-10-23 23:59:59'); // Sales open until 23 October 2017.

// Tab Prices
define('PRICETABS_NUM_TABS',           7);  // Number of
define('PRICETABS_DATE_RANGE',     'P3D');  // 3 days from departure
define('PRICETABS_DATE_INCREMENT', 'P1D');  // 1 day period

// Adults and children defines
define('MINADT', 1);
define('MAXADT', 9);
define('MINCHD', 0);
define('MAXCHD', 8);
define('MININF', 0);
define('MAXINF', 9);
define('MAXPAX', 9);

define('JOURNEY_RETURN',  2);
define('JOURNEY_ONEWAY',  1);

define('OUTBOUND', 0);
define('INBOUND',  1);

define('CACHE_DAILY', 0); // 24*60);
