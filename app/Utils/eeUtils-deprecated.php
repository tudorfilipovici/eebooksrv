<?php

use App\Models\ScreenText;
use App\Models\PartnerConfig;

/**
 * Partner configuration
 * @return PartnerConfig
 *
function eeConfig() {
  $partnerCode = config('client.partner_code');
  $cacheKey    = implode('-', ['partnerConfig', $partnerCode]);
  return Cache::remember($cacheKey, CACHE_DAILY, function() use($partnerCode) {
    return PartnerConfig::find($partnerCode);  
  });
}*/

/**
 * Loads a list of text ids and text from the DB according to
 * parameter provided
 *
 * @param (string) $lang   the 2 letter language code to be used
 * @param (string) $script usually the function/screen
 *
 * @return Array
 */
function eeGetTextArray($lang, $script)
{
  $config     = PartnerConfig::get();
  $clientCode = $config->text;
  $cacheKey   = implode('-', [ 
    'screenText',
    $lang,
    $script,
    $clientCode
  ]);

  $text = Cache::remember($cacheKey, CACHE_DAILY, function() use ($lang, $script, $clientCode) {
    $rows = ScreenText::where('client_code', '=', $clientCode)
                      ->where('lang', '=', $lang)
                      ->where('function', $script)
                      ->get();
    $res = [];
    foreach ($rows as $itm) {
      $res[$itm->lang][$itm->function][$itm->text_id] = $itm->text;
    }
    return $res;
  });

  return $text;
}

/**
 * eeGetText
 *
 * Gets language dependent screen text from database
 *
 * @param (string) $lang     user language (mandatory)
 * @param (string) $script   script or function to which the text applies (mandatory)
 * @param (string) $textcode internal text id (mandatory)
 * @param (string) $search   parameter (or array of parameters) to be searched and
 * replaced (optional)
 * @param (string) $replace  text (or array of texts) to replace $search (optional,
 * but goes together with $search)
 * @param (char)   $nbsp     if 'Y' then change all spaces to &nbsp; (optional)
 * @param (char)   $cleanup  if 'Y' then backslash all double quotes and condense
 * whitespace
 *
 * returns the text directly
 * @return string
 */
function eeGetText( $lang, $script, $textcode,
                    $search = '', $replace = '', $nbsp = 'N', $cleanup = 'N')
{
  if (empty($textcode)) {
    return '';
  }

  $text = eeGetTextArray($lang, $script);

  if (!isset($text[$lang][$script])) {
    return '';
  }

  // exact match
  if( isset($text[$lang][$script][$textcode])) {
    $result = $text[$lang][$script][$textcode];
  } elseif( $script == 'error') {
    // if errors ... look for generic common codes
    $common = $textcode;
    while ( preg_match('#_#', $common)) {
      $common = preg_replace('#[^_]*_(.*)#', '\\1', $common);
      if( isset($text[$lang][$script][$common])) {
        $result = $text[$lang][$script][$common];
      }
    }
  }

  // if still empty - return error
  if (!isset($result)) {
    if (config('app.debug')) {
      $result = "TEXT MISSING: /$lang/$script/$textcode";
    } else {
      $result = $textcode;
    }

    Log::error('MISSINGTEXT', [$lang, $script, $textcode]); // TODO: Add screen
    return $result;
  }

  // add text for dump ??
  // if( isset($debug['dumptxt'])) {
  //   if( !isset($dumptxt)) {
  //     $dumptxt = array();
  //   }

  //   if( !in_array(array( $lang, $script, $textcode, $result), $dumptxt)) {
  //     $dumptxt[] = array( $lang, $script, $textcode, $result);
  //   }
  // }

  if (!empty($search)) {
    $result = str_replace($search, $replace, $result);
  }

  if ($cleanup == 'Y') {
    $result = preg_replace('#[[:space:]]+#', ' ', $result);
    $result = str_replace("\"", "\\\"", $result);
  }

  if ($nbsp == 'Y') {
    $result = preg_replace('#[[:space:]]#', '&nbsp;', $result);
  }

  return $result;
}

/**
 * Finds an element in an array
 *
 * @param array          $collection   The array to search in
 * @param callable|array $search       A comparator function with prototype myComp( $itm, $key, $collection),
 *                                      can also be a map of keys/values to match
 * @param boolean        $getIndex     When true, returns the collection index instead of the value
 *
 *      $users = [
 *        ['id' => 123, 'first' => 'John', 'last'=>'Doe',   'age'=>33],
 *        ['id' => 456, 'first' => 'Juan', 'last'=>'Perez', 'age'=>32],
 *        ['id' => 678, 'first' => 'Jane', 'last'=>'Doe',   'age'=>31],
 *      ];
 *
 *      // All the following return the second user
 *      eeFind($users, ['id' => 456]);
 *      eeFind($users, ['first' => 'Juan', 'last' => 'Perez']);
 *      eeFind($users, function($usr) { return $usr['id'] === 456; });
 *
 *      // Get index
 *      eeFind($users, ['id' => 456], true); // === 1
 *
 * @return mixed The matching element or the index of the matching element or false if none was found
 */
function eeFind( $collection, $search, $getKey = false)
{
  $isArray    = is_array($search);
  $isCallable = is_callable($search);
  foreach ($collection as $key => $itm) {
    // If search is a map of keys/values, create a custom search function
    if ($isArray && self::matches($itm, $search)) {
      return $getKey ? $key : $itm;

    } elseif($isCallable && $search($itm, $key, $collection) === true) {
      return $getKey ? $key : $itm;
    }
  }

  return false;
}

/**
 * Returns true if a map contains the given key/values
 *
 * @param  mixed $itm    Array hash or object to examine
 * @param  array $attrs  Hash of key/value  attributes to match agains $itm
 * @return boolean       True if all the attributes in $attrs match the given $itm
 *
 *     $user = [
 *       'id'    => 123, 
 *       'first' => 'John', 
 *       'last'  => 'Doe',   
 *       'age'   => 33
 *     ];
 *     eeMatches($user, ['id'    => 123]);                 // true
 *     eeMatches($user, ['first' => 'John', 'age' => 33]); // true
 *
 *     $person = new stdClass();
 *     $person->id    = 123;
 *     $person->first = 'John';
 *     $person->last  = 'Doe';
 *     $person->age   = 33;
 *     eeMatches($person, ['id'    => 123]);                 // true
 *     eeMatches($person, ['first' => 'John', 'age' => 33]); // true
 */
function eeMatches($itm, array $attrs) {
  // Arrays
  if (is_array($itm)) {
    foreach($attrs as $key => $val) {
      if (!array_key_exists($key, $itm) || $itm[$key] != $val) {
        return false;
      }
    }

  // Objects
  } elseif (is_object($itm)) {
    foreach($attrs as $key => $val) {
      if (!isset($itm->$key) || $itm->$key != $val) {
        return false;
      }
    }
  }
  return true;
}

/**
 * eePick
 */
function eePick($itm, $attrs) 
{

  if (is_object($itm)) {
    $res = [];

    if (is_string($attrs) && isset($itm->$attrs)) {
      $res[$attrs] = $itm->$attrs;
      return $res;

    } elseif (is_array($attrs)) {
      foreach ($attrs as $attr) {
        if (isset($itm->$attr)) {
          $res[$attr] = $itm->$attr;
        }
      }
    }
    return $res;

  } elseif (is_array($itm)) {

    return array_filter($itm, function($key) use($attrs, $isString) { 
      if (is_string($attrs)) {
        return $key == $attrs;
      }
      return in_array($key, $attrs); 
    }, ARRAY_FILTER_USE_KEY); 
  }

  return [];
}


/**
 * Returns an iterator function that uses eeMatches
 * @param  array $attrs   Hash of key/value  attributes to match agains every item in the array
 * @return function       The iterator function to be used with one of the array_* functions
 *
 *     $persons  = [ 
 *       ['name' => 'John', 'gender' => 'male'],
 *       ['name' => 'Jane', 'gender' => 'female'],
 *       ['name' => 'Juan', 'gender' => 'male'],
 *     ];
 *
 *     $males = array_filter($persons, eeMatchIterator([
 *       'gender' => 'male',
 *     ]));
 */
function eeMatchIterator($attrs, $flatten = false) {
  return function($itm) use($attrs, $flatten) { 
    $res = self::matches($itm, $attrs); 
    return $flatten ? (count($res) == 1 ? reset($res) : array_values($res)) : $res;
  };
}

/**
 * eePickIterator
 *
 *     $persons  = [ 
 *       ['name' => 'John', 'gender' => 'male'],
 *       ['name' => 'Jane', 'gender' => 'female'],
 *       ['name' => 'Juan', 'gender' => 'male'],
 *     ];
 *
 *     $males = array_map(eePickIterator('name', true), $persons); // ['John', 'Jane', 'Juan']
 *     
 */
function eePickIterator($attrs, $flatten = false) {
  return function($itm) use ($attrs, $flatten) { 
    $res = self::pick($itm, $attrs);  
    return $flatten ? (count($res) == 1 ? reset($res) : array_values($res)) : $res;
  };
}
