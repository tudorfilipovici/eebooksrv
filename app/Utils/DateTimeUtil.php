<?php
namespace App\Utils;

use DateTime;
use DateTimeZone;
use DateInterval;
use Exception;

use App\Models\Airport;

class DateTimeUtil {

  /**
   * Creates a DateTime object
   *
   * @param mixed $selDate DATE_NOW, date string or DateTime object
   * @param bool|string $interval A string with the desired interval or false if none
   * @param bool $invert true if the interval is to be subtracted from the date
   * @param bool|string $timeZone String with timezone
   * @return DateTime or false in case of parsing error
   */
  static function create($selDate=DATE_NOW, $interval=false, $invert=false, $timeZone=false)
  {
    $date = new DateTime();

    if( $timeZone === false) {
      $timeZone = $date->getTimezone();
    }

    if( is_string( $timeZone)) {
      try {
        $timeZone = new DateTimeZone($timeZone);
      } catch(Exception $e) {/* Use default timezone */}
    }

    // Date as string
    if( is_string( $selDate)) {
      try {
        $date = new DateTime($selDate, $timeZone);
      } catch(Exception $err) {
        $date = false;
      }

    // From another object
    } elseif( $selDate instanceof DateTime) {
      $date = clone $selDate;
    }

    if( $date && $interval) {
      $interval = new DateInterval($interval);
      $interval->invert = ($invert? 1 : 0);
      $date->add( $interval);
    }

    return $date;
  }

  /**
   * Validates the date is valid for a departure
   *
   * @param bool              $useDepTime Defaults to false. If true, it will take into account the time set in $departure
   * @return bool
   *
   */
  static function isValidDeparture($departure, $return = null, $useDepTime = false)
  {
    if (!$departure instanceof DateTime) {
      return 'INVALID';
    }

    $isRoundTrip = ($return instanceof DateTime);
    // Calculate min departure
    $minDep = DateTimeUtil::create( DATE_NOW, DATE_MIN_DEPARTURE);
    // Calculate max departure according to current return
    if( $isRoundTrip) {
      $maxDep = DateTimeUtil::create( $return, DATE_MIN_RETURN, true);
    } else {
      $maxDep = DateTimeUtil::create( DATE_SALE_OPEN_UNTIL);
    }

    // Reset the departure time to the latest allowed
    $dep = clone $departure;
    if (!$useDepTime) {
      $dep->setTime(23,59,59);
    }
    if( $dep < $minDep ) {
      return 'TOOEARLY';
    }

    // Reset time to minimum
    if (!$useDepTime) {
      $dep->setTime(0,0,0);
    }
    if( $isRoundTrip && $dep > $maxDep ) {
      return 'TOOLATE';
    }

    return true;
  }

  /**
   * Validates the date is valid for a return
   *
   * @param boolean  $useTime   True if the current dates include time info, otherwise time will be ignored
   * @return bool
   */
  static function isValidReturn($return, $departure, $useTime = false)
  {
    if (!$return instanceof DateTime) {
      return 'INVALID';
    }

    if (!$departure instanceof DateTime) {
      return 'INVALID';
    }

    $ret = clone $return;
    $dep = clone $departure;

    if( !$useTime) {
      $ret->setTime(23,59,59);
      $dep->setTime( 0, 0, 0);
    }
    // Calculate min return according to return date
    $minRet = DateTimeUtil::create($dep, DATE_MIN_RETURN);

    // Calculate max return
    $maxRet = DateTimeUtil::create(DATE_SALE_OPEN_UNTIL);

    if( $ret < $minRet) {
      return 'TOOEARLY';
    }

    if( $ret > $maxRet) {
      return 'TOOLATE';
    }

    return true;
  }

  /**
   * TODO: Add cache
   * @param  [type] $code [description]
   * @return [type]       [description]
   */
  static function getTimeZone($code)
  {

    // check for cached version
    // $keys = ['timezone', $code];
    $city = Airport::where('airport_code', '=', $code)->first()->city;
    if ($city) {
      return $city->tz;
    }

    $city = City::where('city_code', '=', $code)->first();
    if ($city) {
      return $city->tz;
    }

    return '';
  //   $query = "
  // SELECT
  //  c.tz
  // FROM
  //  cat_city c INNER JOIN cat_airport a ON c.city_code = a.city_code
  // WHERE
  // c.city_code='$code'
  // UNION SELECT
  //  c.tz
  // FROM
  //  cat_city c INNER JOIN cat_airport a ON c.city_code = a.city_code
  // WHERE
  // a.airport_code='$code'";

  //   list( $numrows, $tz) = eeMysqlRunQuery( $query);
  //   if( !$numrows) {
  //     $serverzone = date_default_timezone_get();
  //     $log['file'] = LOGFILEERR;
  //     $log['type'] = "ILLEGAL CITYCODE";
  //     $log['text'] = "No timezone setting found for citycode: '$code', using server timezone ($serverzone) instead";
  //     eeGenerateLogEntry( $log);
  //     return $serverzone;
  //   }

  //   $timezone = $tz[0][0];
  //   eeWriteCache( 'zz', 'ZZZ', 'ZZ', $keys, $vars);

  // return $timezone;    
  }

  static function daysDiff(DateTime $date1, DateTime $date2)
  {
    $dt1 = DateTime::createFromFormat(DATE_FORMAT_DE, $date1->format(DATE_FORMAT_DE));
    $dt2 = DateTime::createFromFormat(DATE_FORMAT_DE, $date2->format(DATE_FORMAT_DE));
    $interval = $dt1->diff($dt2);
    return $interval->invert ? -$interval->days : $interval->days;
  }
}