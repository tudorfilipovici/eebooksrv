<?php

function bind(array $callable)
{
    return function () use ($callable) {
        call_user_func_array($callable, func_get_args());
    };
}
