<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class PartnerConfig extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cat_partner_config';
  
  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   **/
  public $timestamps = false;

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'partner_code';

  /**
   * Loads the current partner config
   * @return [type] [description]
   */
  static function get($param = null) {
    $partnerCode = config('client.partner_code');
    $cacheKey    = implode('-', ['partnerConfig', $partnerCode]);

    $config = Cache::remember($cacheKey, CACHE_DAILY, function() use($partnerCode) {
      return PartnerConfig::find($partnerCode);  
    });

    return $param  && isset($config->$param) ? $config->$param : $config;
  }
}
