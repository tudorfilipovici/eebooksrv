<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
  use Traits\HasCompositePrimaryKey;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cat_city';
  
  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   **/
  public $timestamps = false;

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = ['city_code', 'country_code'];

  public function names() 
  {
    return $this->hasMany('App\Models\CityName', 'city_code', 'city_code');

  }

  public function country() 
  {
    return $this->hasOne('App\Models\Country', 'country_code', 'country_code');
  }

  public function countryNames() 
  {
    return $this->hasMany('App\Models\CountryName', 'country_code', 'country_code');
  }
}
