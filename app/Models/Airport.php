<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Airport extends Model
{
  use Traits\HasCompositePrimaryKey;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cat_airport';
  
  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   **/
  public $timestamps = false;

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = ['airport_code', 'city_code'];

  // public function name($lang)
  // {
  //   return AirportName::where('airport_code', $this->airport_code)->where('lang', $lang)->get('airport_name');
  // }
  
  public function names()
  {
    return $this->hasMany('App\Models\AirportName', 'airport_code', 'airport_code');
  }

  public function city()
  {
    return $this->hasOne('App\Models\City', 'city_code', 'city_code');
  }

  /**
   * Retrieves the airport details
   * 
   * @param  string $lang        [description]
   * @param  string $airportCode [description]
   * @return Collection           
   *           - airportCode
   *           - airportName
   *           - cityCode
   *           - cityName
   *           - countryCode
   *           - countryName
   */
  static function get($lang, $airportCode)
  {
    $whereLang = function($query) use($lang) { $query->where('lang', $lang); };

    $query = Airport::where('airport_code', '=', $airportCode)
      ->with([
        'names'             => $whereLang,
        'city.names'        => $whereLang,
        'city.countryNames' => $whereLang,
      ]);

    $cacheKey = implode('-', [
      'airportDetails',
      $lang,
      $airportCode,
    ]);

    $data = Cache::remember($cacheKey, CACHE_DAILY, function() use ($query) {
      return $query->first();
    });

    if (!$data) {
      Log::error('MISSING_AIRPORT_INFO', [$lang, $airportCode]);
      return new Collection([
        'airportCode'     => $airportCode,
      ]);
    }

    return new Collection([
      'airportCode'     => $data->airport_code,
      'airportName'     => $data->names[0]->airport_name,
      'cityCode'        => $data->city_code,
      'cityName'        => $data->city->names[0]->city_name,
      'countryCode'     => $data->city->country_code,
      'countryName'     => $data->city->countryNames[0]->country_name,
    ]);
  }

  static function getFullName($lang, $airportCode) {
    $data = Airport::get($lang, $airportCode);
    $airportName = $data->get('airportName');
    $airportCode = $data->get('airportCode');
    $countryName = $data->get('countryName');
    if (!$airportCode) {
      return '';
    }
    return "$airportName ($airportCode) - $countryName";
  }
}
