<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Models\PartnerConfig;

class ScreenText extends Model
{
  use Traits\HasCompositePrimaryKey;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cat_screen_text';
  
  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   **/
  public $timestamps = false;

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = ['client_code', 'lang', 'module', 'function', 'text_id'];

  /**
   * Loads a list of text ids and text from the DB according to
   * parameter provided
   *
   * @param (string) $lang   the 2 letter language code to be used
   * @param (string) $script usually the function/screen
   *
   * @return Array
   */
  static function getArray($lang, $script)
  {
    $clientCode = PartnerConfig::get('text');
    $cacheKey   = implode('-', [ 
      'screenText',
      $lang,
      $script,
      $clientCode
    ]);

    $text = Cache::remember($cacheKey, CACHE_DAILY, function() use ($lang, $script, $clientCode) {
      $rows = ScreenText::where('client_code', '=', $clientCode)
                        ->where('lang', '=', $lang)
                        ->where('function', $script)
                        ->get();
      $res = [];
      foreach ($rows as $itm) {
        $res[$itm->lang][$itm->function][$itm->text_id] = $itm->text;
      }
      return $res;
    });

    return $text;
  }

  /**
   *
   * Gets language dependent screen text from database
   *
   * @param (string) $lang     user language (mandatory)
   * @param (string) $script   script or function to which the text applies (mandatory)
   * @param (string) $textcode internal text id (mandatory)
   * @param (string) $search   parameter (or array of parameters) to be searched and
   * replaced (optional)
   * @param (string) $replace  text (or array of texts) to replace $search (optional,
   * but goes together with $search)
   * @param (char)   $nbsp     if 'Y' then change all spaces to &nbsp; (optional)
   * @param (char)   $cleanup  if 'Y' then backslash all double quotes and condense
   * whitespace
   *
   * returns the text directly
   * @return string
   */
  static function get( $lang, $script, $textcode,
                      $search = '', $replace = '', $nbsp = 'N', $cleanup = 'N')
  {
    if (empty($textcode)) {
      return '';
    }

    $text = ScreenText::getArray($lang, $script);

    if (!isset($text[$lang][$script])) {
      return '';
    }

    // exact match
    if( isset($text[$lang][$script][$textcode])) {
      $result = $text[$lang][$script][$textcode];
    } elseif( $script == 'error') {
      // if errors ... look for generic common codes
      $common = $textcode;
      while ( preg_match('#_#', $common)) {
        $common = preg_replace('#[^_]*_(.*)#', '\\1', $common);
        if( isset($text[$lang][$script][$common])) {
          $result = $text[$lang][$script][$common];
        }
      }
    }

    // if still empty - return error
    if (!isset($result)) {
      if (config('app.debug')) {
        $result = "TEXT MISSING: /$lang/$script/$textcode";
      } else {
        $result = $textcode;
      }

      Log::warning('MISSINGTEXT', [$lang, $script, $textcode]); // TODO: Add screen
      return $result;
    }

    // add text for dump ??
    // if( isset($debug['dumptxt'])) {
    //   if( !isset($dumptxt)) {
    //     $dumptxt = array();
    //   }

    //   if( !in_array(array( $lang, $script, $textcode, $result), $dumptxt)) {
    //     $dumptxt[] = array( $lang, $script, $textcode, $result);
    //   }
    // }

    if (!empty($search)) {
      $result = str_replace($search, $replace, $result);
    }

    if ($cleanup == 'Y') {
      $result = preg_replace('#[[:space:]]+#', ' ', $result);
      $result = str_replace("\"", "\\\"", $result);
    }

    if ($nbsp == 'Y') {
      $result = preg_replace('#[[:space:]]#', '&nbsp;', $result);
    }

    return $result;
  }

  /**
   * Returns an array with all available languages
   * @return array
   */
  static function languages() {
    $clientCode = PartnerConfig::get('text');
    $cacheKey   = implode('-', [ 
      'screenText::languages',
      $clientCode
    ]);

    $langs = Cache::remember($cacheKey, CACHE_DAILY, function() use ($clientCode) {    
      return ScreenText::distinct()->select('lang')->where('client_code', '=', $clientCode)->get();
    });

    return $langs->map(function($itm) { return $itm->lang; });
  } 
}
