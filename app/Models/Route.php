<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\PartnerConfig;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Route extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cat_routes';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   *
  public $timestamps = false;*/

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'route_id';

  /**
   * The storage format of the model's date columns.
   *
   * @var string
   */
  protected $dateFormat = '';

  /**
   * The name of the "created at" column.
   *
   * @var string
   */
  const CREATED_AT = 'created_timestamp';

  /**
   * The name of the "updated at" column.
   *
   * @var string
   */
  const UPDATED_AT = 'modified_timestamp';

  public function originAirport()
  {
    return $this->hasOne('App\Models\Airport', 'airport_code', 'origin');
  }

  public function destinAirport()
  {
    return $this->hasOne('App\Models\Airport', 'airport_code', 'destination');
  }

  public function scopeAirportDetails($query, $lang, $origin = false, $destin = false) {
    $select = $query->select(DB::raw(implode(',', [
      "{$this->table}.host_carrier_code AS carrier_code",
      'a1.city_code     AS origin_city_code',
      'cn1.city_name    AS origin_city_name',
      'a1.airport_code  AS origin_code',
      'an1.airport_name AS origin_name',
      'a1.latitude      AS origin_lat',
      'a1.longitude     AS origin_lon',
      'a2.airport_code  AS destin_code',
      'an2.airport_name AS destin_name',
      'a2.latitude      AS destin_lat',
      'a2.longitude     AS destin_lon',
      'a2.city_code     AS destin_city_code',
      'cn2.city_name    AS destin_city_name',
      'co1.country_code AS origin_country_code',
      'co1.country_name AS origin_country_name',
      'co2.country_code AS destin_country_code',
      'co2.country_name AS destin_country_name',
    ])));

    // Get airport codes and names
    $query
      ->join('cat_airport AS a1', DB::raw('a1.airport_code'),  '=', "{$this->table}.origin")
      ->join('cat_airport AS a2', DB::raw('a2.airport_code'),  '=', "{$this->table}.destination")
      ->join('cat_airport_name AS an1', function($join) use($lang) {
        $join->on(DB::raw('an1.airport_code'), '=', 'a1.airport_code')
             ->where(DB::raw('an1.lang'), '=', $lang);
      })
      ->join('cat_airport_name AS an2', function($join) use($lang) {
        $join->on(DB::raw('an2.airport_code'), '=', 'a2.airport_code')
             ->where(DB::raw('an2.lang'), '=', $lang);
      });

    // Get city data
    $query
      ->join('cat_city AS c1', DB::raw('c1.city_code'), '=', DB::raw('a1.city_code'))
      ->join('cat_city AS c2', DB::raw('c2.city_code'), '=', DB::raw('a2.city_code'))
      ->join('cat_city_name AS cn1', function($join) use($lang) {
        $join->on(DB::raw('cn1.city_code'), '=', 'c1.city_code')
             ->where(DB::raw('cn1.lang'), '=', $lang);
      })
      ->join('cat_city_name AS cn2', function($join) use($lang) {
        $join->on(DB::raw('cn2.city_code'), '=', 'c2.city_code')
             ->where(DB::raw('cn2.lang'), '=', $lang);
      });

    // Get Country data
    $query
      ->join('cat_country_name AS co1', function($join) use($lang) {
        $join->on(DB::raw('co1.country_code'), '=', 'c1.country_code')
             ->where(DB::raw('co1.lang'), '=', $lang);
      })
      ->join('cat_country_name AS co2', function($join) use($lang) {
        $join->on(DB::raw('co2.country_code'), '=', 'c2.country_code')
             ->where(DB::raw('co2.lang'), '=', $lang);
      });

    // Search by origin
    if ($origin) {
      $query->where(function ($query) use ($origin) {
        $query->orWhere(DB::raw('a1.airport_code'),  '=',    $origin)
              ->orWhere(DB::raw('a1.city_code'),     '=',    $origin)
              ->orWhere(DB::raw('an1.airport_name'), 'like', "%{$origin}%")
              ->orWhere(DB::raw('cn1.city_name'),    'like', "%{$origin}%");
      });
    }

    // Search by destination
    if ($destin) {
      $query->where(function ($query) use ($destin) {
        $query->orWhere(DB::raw('a2.airport_code'),  '=',    $destin)
              ->orWhere(DB::raw('a2.city_code'),     '=',    $destin)
              ->orWhere(DB::raw('an2.airport_name'), 'like', "%{$destin}%")
              ->orWhere(DB::raw('cn2.city_name'),    'like', "%{$destin}%");
      });
    }
  }

  public function scopeActiveRoutes($query, $carrierCode) {
    $query->where('logically_deleted', 0)
          ->where('host_carrier_code', $carrierCode);
  }

  /**
   * Restrict search to exact origin and destination codes
   * @param  [type]  $query  [description]
   * @param  [type]  $origin [description]
   * @param  boolean $destin [description]
   * @return [type]          [description]
   */
  public function scopeOriginDestination($query, $origin, $destin = false) {
    if ($origin) {
      $query->where($this->table.'.origin', '=', $origin);
    }

    if ($destin) {
      $query->where($this->table.'.destination', '=', $destin);
    }
  }

  static function allPartnerRoutes($lang, $origin = false, $destin = false) {
    // -----
    // Example of alternative less performant implementation:
    //
    // $whereLang = function($query) use($lang) { $query->where('lang', $lang); };
    // $routes    = Route::where('logically_deleted', 0)
    //   ->where('host_carrier_code', $carrierCode)
    //   ->with([
    //     'originAirport.names' => $whereLang,
    //     'destinAirport.names' => $whereLang,
    //     'originAirport.city.names' => $whereLang,
    //     'destinAirport.city.names' => $whereLang,
    //     'originAirport.city.countryNames' => $whereLang,
    //     'destinAirport.city.countryNames' => $whereLang,
    //   ]);
    // return $routes->select('route_id','origin','destination')->get();

    $carrierCode = PartnerConfig::get('carriercode');

    // check for cached version
    $cacheKey = implode('-', [
      'partnerRoutes',
      $lang,
      $carrierCode,
      $origin,
      $destin
    ]);

    $query = Route::activeRoutes($carrierCode)
      ->airportDetails($lang, $origin, $destin)
      ->orderBy('origin_name',         'asc')
      ->orderBy('origin_country_name', 'asc')
      ->orderBy('destin_name',         'asc')
      ->orderBy('destin_country_name', 'asc');

    return Cache::remember($cacheKey, CACHE_DAILY, function() use ($cacheKey, $query) {
      Log::debug('DBQUERY', [$cacheKey, $query->toSql()]);
      return $query->get();
    });
  }
}
