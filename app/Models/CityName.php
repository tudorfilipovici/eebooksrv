<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityName extends Model
{
  use Traits\HasCompositePrimaryKey;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cat_city_name';
  
  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   **/
  public $timestamps = false;

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = ['city_code', 'lang'];
}
