<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cat_country';
  
  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   **/
  public $timestamps = false;

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'country_code';

  public function names()
  {
    return $this->hasMany('App\Models\CountryName', 'country_code', 'country_code');
  }
}
