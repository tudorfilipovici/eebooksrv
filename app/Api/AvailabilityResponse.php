<?php 

namespace App\Api;

// use App\Edi\Availability;
// use App\Edi\TabPrice;
// use App\Edi\Flight;
// use App\Edi\FlightLeg;
// use App\Edi\MatrixItem;
// use App\Edi\Tax;
// use App\Edi\TaxElement;
// use App\Edi\Total;
// use App\Edi\Price;
// use App\Models\Airport;
// use App\Models\ScreenText;
// use JsonSerializable;

use App\Edi\Bee\AvailabilityRT as Availability;
use App\Edi\Bee\TabPrice;
use App\Edi\Flight;
use App\Edi\Bee\FlightLeg;
use App\Edi\MatrixItem;
use App\Edi\Tax;
use App\Edi\TaxElement;
use App\Edi\Bee\Total;
use App\Edi\Bee\Price;
use App\Models\Airport;
use App\Models\ScreenText;
use App\Utils\DateTimeUtil;
use \DateInterval;
use JsonSerializable;

/**
 * AvailabilityResponse
 */
class AvailabilityResponse implements JsonSerializable {
  protected $search;
  protected $avail;
  protected $allPrices = [];
  protected $byFlight  = [];

  /**
   * Constructor
   * @param FlightSearch $search [description]
   * @param Availability $avail  [description]
   */
  public function __construct(FlightSearch $search, Availability $avail) {
    $this->search = $search;
    $this->avail  = $avail;

    $this->lang   = config('client.lang');

    // Generate an index with only the valid fares
    foreach($this->avail->matrixItems as $itm) {
      if ($itm->hasError) {
        continue;
      }
      $this->allPrices[] = $itm;

      $this->byFlight[OUTBOUND][$itm->fltRefOut][$itm->compOut][] = $itm;
      $this->byFlight[INBOUND][$itm->fltRefIn][$itm->compIn][]    = $itm;
    }

    $this->compNames = [
      'EC' => ScreenText::get($this->lang, 'compartment', 'EC'),
      'ER' => ScreenText::get($this->lang, 'compartment', 'ER'),
      'EF' => ScreenText::get($this->lang, 'compartment', 'EF'),
      'BR' => ScreenText::get($this->lang, 'compartment', 'BR'),
      // 'BF' => ScreenText::get($this->lang, 'compartment', 'BF'),
    ];
  }

  /**
   * jsonSerialize
   * @return [type] [description]
   */
  public function jsonSerialize() {
    
    $flightsById = [];
    foreach ($this->avail->journeys as $jix => $journey) {
      foreach ($journey->flights as $fix => $flight) {
        $flightsById["$jix-$fix"] = $this->jsonSerializeFlight($flight);
      }
    }

    $tabs = [];
    foreach ($this->avail->tabPrices as $jix => $tabPrices) {
      if (empty($tabPrices)) {
        continue;
      }
      $dir = $jix === OUTBOUND ? 'outbounds' : 'inbounds';      
      $tabPrices  = $this->normalizeTabPrices($jix, $tabPrices, $this->avail->getRequest());
      $tabs[$dir] = array_map([$this, 'jsonSerializeTabPrice'], $tabPrices);
    }

    return [
      'search'       => $this->search->jsonSerialize(),
      'tabs'         => $tabs,
      'compartments' => $this->compNames,
      'initial'      => $this->getInitialState(),
      'options'      => $this->jsonSerializeFares(),
      'flightsById'  => $flightsById,
      'taxesById'    => array_map([$this, 'jsonSerializeTax'],   $this->avail->taxes),
      'totalsById'   => array_map([$this, 'jsonSerializeTotal'], $this->avail->totals),
      'faresById'    => array_map([$this, 'jsonSerializeFare'],  $this->avail->prices),
    ];
  }

  protected function getInitialState() {
    $flights = [];
    
    // Initialize flights
    foreach ($this->avail->journeys as $jix => $journey) {
      foreach ($journey->flights as $fix => $flight) {
        $dir    = $jix == OUTBOUND ? 'outbounds' : 'inbounds';
        $prices = $this->getCheapestByComp($jix, $fix);
        $ids    = array_fill(0, count($prices), $jix);
        $flights[$dir] = [
          'id'     => "$jix-$fix",
          // 'jix'    => $jix,
          // 'fix'    => $fix,
          'prices' => array_map([$this,'jsonSerializeMatrixItem'], $ids, $prices),
        ];
      }
    }

    return $flights;
  }

  protected function getCheapestByComp($jix, $fix) {
    $results = [];
    $pricesByComp = $this->byFlight[$jix][$fix];
    foreach ($pricesByComp as $comp => $prices) {

      $cheapest = null;
      array_reduce($prices, function($total, $itm) use(&$cheapest) {
        $currTotal = $itm->getTotal()->amount;
        if ($total === null || $total > $currTotal) {
          $cheapest = $itm;
          return $currTotal;
        }        
        return $total;
      });

      if ($cheapest) {
        $results[] = $cheapest;
      }
    }
    return $results;
  }

  /**
   * jsonSerializeTabPrice
   * @param  TabPrice $tabPrice [description]
   * @return [type]             [description]
   */
  protected function jsonSerializeTabPrice(TabPrice $tabPrice) {
    return [
      'date'     => $tabPrice->getDate(DATE_FORMAT_DE),
      'active'   => $tabPrice->active,
      'info'     => $tabPrice->info,
      'amount'   => intval($tabPrice->amount),
      'currency' => $tabPrice->currency,
    ];
  }

  /**
   * jsonSerializeFlight
   * @param  Flight $flight [description]
   * @return [type]         [description]
   */
  protected function jsonSerializeFlight(Flight $flight) {
    $firstLeg = $flight->firstAirLeg();
    $lastLeg  = $flight->lastAirLeg();
    // $origin   = Airport::get($this->lang, $firstLeg->originCode);
    // $destin   = Airport::get($this->lang, $lastLeg->destinCode);

    return [
      // 'id'                 => $jix.'-'.$flight->id,
      'originCode'         => $firstLeg->originCode,
      'originName'         => Airport::getFullName($this->lang, $firstLeg->originCode),
      'destinCode'         => $lastLeg->originCode,
      'destinName'         => Airport::getFullName($this->lang, $lastLeg->destinCode),
      'departure'          => $firstLeg->departure(DATE_FORMAT_DE),
      'arrival'            => $lastLeg->arrival(DATE_FORMAT_DE),
      'departureUTC'       => $firstLeg->departure(DATETIME_FORMAT_ISO8601),
      'arrivalUTC'         => $lastLeg->arrival(DATETIME_FORMAT_ISO8601),
      'duration'           => $flight->duration(),
      'legs'               => array_map([$this, 'jsonSerializeLeg'], array_values($flight->legs)),
      // 'originCode'         => $origin->get('airportCode'),
      // 'originName'         => $origin->get('airportName'),
      // 'originCountry'      => $origin->get('countryCode'),
      // 'destinationCode'    => $destin->get('airportCode'),
      // 'destinationName'    => $destin->get('airportName'),
      // 'destinationCountry' => $destin->get('countryCode'),
    ];
  }

  /**
   * jsonSerializeLeg
   * @param  FlightLeg $leg [description]
   * @return [type]         [description]
   */
  protected function jsonSerializeLeg(FlightLeg $leg) {
    return [
      'fltnum'       => $leg->flightNumber(),
      'operator'     => $leg->operatorCode,
      'equipment'    => $leg->equipmentCode,
      'originCode'   => $leg->originCode, 
      'originName'   => Airport::getFullName($this->lang, $leg->originCode), 
      'destinCode'   => $leg->destinCode, 
      'destinName'   => Airport::getFullName($this->lang, $leg->destinCode),
      'departure'    => $leg->departure(DATE_FORMAT_DE),
      'arrival'      => $leg->arrival(DATE_FORMAT_DE),
      'departureUTC' => $leg->departure(DATETIME_FORMAT_ISO8601),
      'arrivalUTC'   => $leg->arrival(DATETIME_FORMAT_ISO8601),
      'duration'     => $leg->duration,
    ];
  }

  /**
   * jsonSerializeFares
   * @return [type] [description]
   */
  protected function jsonSerializeFares() {
    $isRoundTrip = $this->avail->isRoundTrip();
    $prices      = [];

    foreach ($this->allPrices as $itm) {
      $prices[] = [
        'outboundPrice' => $this->jsonSerializeMatrixItem(OUTBOUND, $itm),
        'inboundPrice'  => $isRoundTrip ? $this->jsonSerializeMatrixItem(INBOUND,  $itm) : null,
        'totalTax'      => $itm->getTax()->id,
        'totalPrice'    => $itm->getTotal()->id,
      ];
    }

    return $prices;
  }

  /**
   * [jsonSerializeMatrixItem
   * @param  MatrixItem $itm [description]
   * @return array           [description]
   */
  protected function jsonSerializeMatrixItem($jix, MatrixItem $itm) {
    $flight = $itm->getFlight($jix);
    $comp   = $itm->getComp($jix);
    $fare   = $itm->getFare($jix);

    return [
      'id'              => "{$jix}-{$flight->id}-{$comp}",
      // 'jix'             => intval($jix),
      // 'fix'             => intval($flight->id),
      'compCode'        => $comp,
      // 'compName'        => $this->compNames[$comp],
      'flightId'        => $flight->id,
      'priceId'         => $fare->id
    ];
  }

  protected function jsonSerializeTax(Tax $tax) {
    return [
      'id'        => $tax->id,
      'adt'       => $tax->adt,
      'chd'       => $tax->chd,
      'inf'       => $tax->inf,
      'currency'  => $tax->currency,
      // 'breakdown' => array_map([$this, 'jsonSerializeTaxItem'], $tax->breakdown)
    ];
  } 

  // protected function jsonSerializeTaxItem(TaxElement $taxEl) {
  //   return [
  //     'code'      => $taxEl->code,
  //     'adt'       => $taxEl->adt,
  //     'chd'       => $taxEl->chd,
  //     'inf'       => $taxEl->inf,
  //     'currency'  => $taxEl->currency,      
  //   ];
  // }

  protected function jsonSerializeTotal(Total $total) {
    return [
      'id'       => $total->id,
      'amount'   => $total->amount,
      'discount' => $total->discount,
      'currency'  => $total->currency,
    ];
  } 

  protected function jsonSerializeFare(Price $fare) {
    return [
      'id'            => $fare->id,
      'fareAmountAdt' => $fare->fareAmountAdt,
      'fareAmountChd' => $fare->fareAmountChd,
      'fareAmountInf' => $fare->fareAmountInf,
      'currency'      => $fare->currency,
      'fareClass'     => $fare->fareClass,
      'bookingClass'  => $fare->bookingClass,
    ];
  } 


  /**
   * From the selected journey $jix, creates an array
   * with the dates range to display as tabs and the associated tab info if available
   *
   * @return array
   */
  protected function normalizeTabPrices($jix, $pricetabs, $req)
  {
    if ($jix == OUTBOUND) {
      $inpDate = $req->departureDate();
    } else {
      $inpDate = $req->returnDate();
    }

    // Move the selected date to the past and start building the tabs
    $date       = DateTimeUtil::create($inpDate, PRICETABS_DATE_RANGE, true);
    $day        = new DateInterval(PRICETABS_DATE_INCREMENT);
    $availDates = array();
    $departure  = $req->departureDate();

    // Print the tabs and increment the date as configured
    for ($i = 0; $i < PRICETABS_NUM_TABS; ++$i, $date->add($day)) {

      $tbPrice = null;
      // Validate the date is within the allowed range
      if (DateTimeUtil::daysDiff($date, $inpDate) === 0 ||
          ($jix == OUTBOUND && DateTimeUtil::isValidDeparture($date) === true) ||
          ($jix == INBOUND  && DateTimeUtil::isValidReturn($date, $departure) === true)) {

        // Search if the current date is in the array of tabs
        foreach ($pricetabs as $tab) {
          if (DateTimeUtil::daysDiff($tab->getDate(), $date) === 0) {
            $tbPrice = $tab;
            break;
          }
        }
      }

      if (!$tbPrice) {
        $tbPrice = new TabPrice([
          'isOutbound' => ($jix == OUTBOUND),
          'info'       => TabPrice::INFO_INVALID,
          'date'       => clone $date ,
        ]);
      } else {
        $tbPrice = clone $tbPrice;
      }

      // Pack the info
      $tbPrice->setActive(DateTimeUtil::daysDiff($date, $inpDate) == 0);

      // If there are no available fares to select
      // Do not display the price in the active tab
      // if ($tbPrice->isActive()) {
      //   $tbPrice->info     = TabPrice::INFO_SOLDOUT;
      //   $tbPrice->amount   = '';
      //   $tbPrice->currency = '';
      // }

      $availDates[] = $tbPrice;
    }

    return $availDates;
  }
}
