<?php

namespace App\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Validation\ValidatesWhenResolvedTrait;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

use Exception;
use Validator as ValidatorFactory;
use JsonSerializable;

/**
 * Base class for all our requests
 * Loosely based in FormRequest from laravel
 */
abstract class ApiRequest implements ValidatesWhenResolved, JsonSerializable
{
  use ValidatesWhenResolvedTrait;

  public function __get($field) 
  {
    if (!isset($this->$field)) {
      throw new Exception("Field '$field' does not exists");
    }
    return $this->$field;
  }

  /**
   * Creates an object from an HttpRequest, throws error if validation fails
   * @param  Request $request Request data
   * @return FlightSearch     The created instance
   */
  static function fromRequest(Request $request) {
    $model = new static();
    $model->loadRequest($request);
    $model->validate();
    return $model;
  }

  /**
   * Get the validator instance for the request.
   *
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function getValidatorInstance()
  {
    $validator = ValidatorFactory::make(
      $this->validationData(),
      $this->rules(),
      $this->messages(), 
      $this->attributes()
    );

    return $validator;
  }

  /**
   * Handle a failed validation attempt.
   *
   * @param  \Illuminate\Contracts\Validation\Validator  $validator
   * @return void
   *
   * @throws \Illuminate\Validation\ValidationException
   */
  protected function failedValidation(Validator $validator)
  {
    throw new ValidationException($validator, $this->response([
      'errors' => $this->formatErrors($validator)
    ]));
  }

  /**
   * Determine if the request passes the authorization check.
   *
   * @return bool
   */
  protected function authorize()
  {
    return true;
  }

  /**
   * Handle a failed authorization attempt.
   *
   * @return void
   *
   * @throws \Illuminate\Http\Exception\HttpResponseException
   */
  protected function failedAuthorization()
  {
    throw new HttpResponseException($this->forbiddenResponse());
  }

  /**
   * Get the proper failed validation response for the request.
   *
   * @param  array  $errors
   * @return \Symfony\Component\HttpFoundation\Response
   */
  protected function response(array $errors)
  {
    return new JsonResponse($errors, 422);
  }

  /**
   * Get the response for a forbidden operation.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  protected function forbiddenResponse()
  {
    return new Response('Forbidden', 403);
  }

  /**
   * Format the errors from the given Validator instance.
   *
   * @param  \Illuminate\Contracts\Validation\Validator  $validator
   * @return array
   */
  protected function formatErrors(Validator $validator)
  {
    return $validator->getMessageBag()->toArray();
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  protected function messages()
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  protected function attributes()
  {
    return [];
  }

  /**
   * TODO: Add translations for generic rules
   *
   * @return array
   */
  protected function rules() { 
    return []; 
  }

  /**
   * Get data to be validated from the request.
   *
   * @return array
   */
  protected abstract function validationData(); // { return []; }
  public    abstract function loadRequest(Request $req); // { return $this; }
  public    abstract function jsonSerialize(); // { return []; }
}
