<?php

namespace App\Api;

use App\Utils\DateTimeUtil;
use App\Models\Route;
// use App\Models\PartnerConfig;
use App\Models\ScreenText;
// use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Arr;
use JsonSerializable;

/**
 * https://scotch.io/tutorials/debugging-queries-in-laravel
 */
class FlightRoutes implements JsonSerializable {

  public function __construct($lang, $routes) {
    $this->lang   = $lang;
    $this->routes = $this->calculateRoutes($routes);
  }

  public function JsonSerialize() {
    $minDeparture = DateTimeUtil::create(DATE_NOW, DATE_MIN_DEPARTURE);
    $minReturn    = DateTimeUtil::create($minDeparture, DATE_MIN_RETURN);
    $maxReturn    = DateTimeUtil::create(DATE_SALE_OPEN_UNTIL);
    $maxDeparture = DateTimeUtil::create($maxReturn, DATE_MIN_RETURN, -1);

    return [
      'routes'    => $this->routes['results'],
      'airports'  => $this->routes['airports'],
      'countries' => $this->routes['countries'],
      'dates'  => [
        'departure' => [
          'periodStart' => $minDeparture->format(DATE_FORMAT_DE),
          'periodEnd'   => $maxDeparture->format(DATE_FORMAT_DE),
        ],
        'return' => [
          'periodStart' => $minReturn->format(DATE_FORMAT_DE),
          'periodEnd'   => $maxReturn->format(DATE_FORMAT_DE),
        ]
      ],
      'passengers' => [
        'maxAdults'   => MAXADT,
        'maxChildren' => MAXCHD,
        'maxInfants'  => MAXINF,
      ],
    ];
  }

  protected function calculateRoutes($routes) {
    $routes = $this->normalizeRoutes($routes);
    $this->addGlobalCityRoutes($routes, 'origin', 'destin');
    $this->addGlobalCityRoutes($routes, 'destin', 'origin');

    $airports  = [];
    $countries = [];

    $items        = [];
    $timestamp = date(DATE_FORMAT_DE);
    foreach ($routes as $route) {
      $key = $route['origin']['airport_code'] . '-' . $route['destin']['airport_code'];
      $items[$key] = [
        'destinationCountryCode'  => $route['destin']['country_code'],
        // 'destinationCountryName'  => $route['destin']['country_name'],
        'destinationCode'         => $route['destin']['airport_code'],
        // 'destinationName'         => $route['destin']['airport_name'],
        // 'destinLat'               => $route['destin']['destin_lat'],
        // 'destinLon'               => $route['destin']['destin_lon'],
        'originCountryCode'       => $route['origin']['country_code'],
        // 'originCountryName'       => $route['origin']['country_name'],
        'originCode'              => $route['origin']['airport_code'],
        // 'originName'              => $route['origin']['airport_name'],
        // 'originLat'               => $route['origin']['origin_lat'],
        // 'originLon'               => $route['origin']['origin_lon']
      ];

      $this->addAirport($airports,  $route['origin']);
      $this->addAirport($airports,  $route['destin']);
      $this->addCountry($countries, $route['origin']);
      $this->addCountry($countries, $route['destin']);
    }

    return [
      'results'   => array_values($items),
      'airports'  => $airports,
      'countries' => $countries
    ];
  }

  protected function addAirport(array &$airports, $route) {
    $code = $route['airport_code'];
    if (!isset($airports[$code])) {
      $airports[$code] = [
        'name'      => $route['airport_name'],
        'latitude'  => $route['latitude'],
        'longitude' => $route['longitude'],
      ];
    }
  }

  protected function addCountry(array &$countries, $route) {
    $code = $route['country_code'];
    if (!isset($countries[$code])) {
      $countries[$code] = [
        'name' => $route['country_name'],
      ];
    }    
  }

  protected function normalizeRoutes($routes) {
    return $routes->map(function($itm) {

      $orig = [];
      $orig['city_code']    = $itm->origin_city_code;
      $orig['city_name']    = $itm->origin_city_name;
      $orig['airport_code'] = $itm->origin_code;
      $orig['airport_name'] = $itm->origin_name;
      $orig['country_code'] = $itm->origin_country_code;
      $orig['country_name'] = $itm->origin_country_name;
      $orig['latitude']     = $itm->origin_lat;
      $orig['longitude']    = $itm->origin_lon;

      $dest = [];
      $dest['city_code']    = $itm->destin_city_code;
      $dest['city_name']    = $itm->destin_city_name;
      $dest['airport_code'] = $itm->destin_code;
      $dest['airport_name'] = $itm->destin_name;
      $dest['country_code'] = $itm->destin_country_code;
      $dest['country_name'] = $itm->destin_country_name;
      $dest['latitude']     = $itm->destin_lat;
      $dest['longitude']    = $itm->destin_lon;

      return [
        'origin' => $orig,
        'destin' => $dest
      ];
    });
  }

  /**
   * Adds an 'All airports' option to the routes with more than one airport
   * @param array  $routes
   * @param string $dir
   */
  protected function addGlobalCityRoutes($routes, $from, $to)
  {
    $groupByCity = [];
    foreach( $routes as $route) {
      $orig = $route[$from]['city_code'];
      $arpt = $route[$from]['airport_code'];
      $groupByCity[$orig][$arpt][$from] = $route[$from];
      $groupByCity[$orig][$arpt][$to][] = $route[$to];
    }

    // Add a general destination option to each city group
    foreach( $groupByCity as $cityCode => &$cityRoutes ) {
      if( count($cityRoutes) < 2) {
        continue;
      }

      // Skip those cities which have an airport code
      // which is the same as the city name
      $cityAirport = Arr::first($cityRoutes, function($route) use($cityCode, $from) {
        return $route[$from]['airport_code'] == $cityCode;
      });

      if( $cityAirport) {
        continue;
      }

      foreach( $cityRoutes as $route) {

        $orig = array_merge($route[$from], array(
          'airport_name' => $route[$from]['city_name'].' - '.ScreenText::get($this->lang, 'general', 'All_airports'),
          'airport_code' => $route[$from]['city_code'],
          'global'       => true,
        ));

        foreach( $route[$to] as $destin) {
          $itm = [];
          $itm[$from] = $orig;
          $itm[$to]   = $destin;
          $routes[]   = $itm;
        }
      }
    }
  }
}
