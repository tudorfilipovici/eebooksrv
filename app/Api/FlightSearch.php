<?php 

namespace App\Api;

use App\Models\ScreenText;
use App\Models\Airport;
use App\Utils\DateTimeUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Exception;
use DateTime;

/**
 * This class is used to hold the user's search selection
 */
class FlightSearch extends ApiRequest {
  protected $numAdt      = MINADT;         // number of adult
  protected $numChd      = MINCHD;         // number of chd
  protected $numInf      = MININF;         // number of inf
  protected $numJourneys = JOURNEY_RETURN; // 1=oneway, 2=return
  protected $originCode  = '';             // origin Airport code
  protected $destinCode  = '';             // destination Airport Code
  protected $railFly     = false;          // railfly
  protected $departure   = null;           // Date and time of min departure
  protected $return      = null;           // Date and time of max return

  /**
   * Creates an object with default info
   * @return FlightSearch     The created instance
   */
  static function fromDefaults() {
    $model = new static();
    // Load default info
    $model->originCode = MLD_AIRPORT;
    $model->departure  = DateTimeUtil::create(DATE_NOW, DATE_DEPARTURE_DEFAULT);
    $model->return     = DateTimeUtil::create(DATE_NOW, DATE_RETURN_DEFAULT);
    return $model;
  }

  public function loadRequest(Request $request) 
  {
    $this->numAdt      = intval($request->input('numAdt', MINADT));
    $this->numChd      = intval($request->input('numChd', MINCHD));
    $this->numInf      = intval($request->input('numInf', MININF));
    $this->originCode  = $request->input('originCode');
    $this->destinCode  = $request->input('destinCode');
    // $this->railFly     = $request->input('railFly') == true;
    $this->departure   = DateTime::createFromFormat(DATE_FORMAT_DE, $request->input('departure'));
    
    if ($request->input('return')) {
      $this->return = DateTime::createFromFormat(DATE_FORMAT_DE, $request->input('return'));
    } else {
      $this->return = null;
    }

    $this->numJourneys = $this->return === null ? JOURNEY_ONEWAY : JOURNEY_RETURN;
    // intval($request->input('numJourneys', JOURNEY_RETURN));

    return $this;
  }

  public function originName() {
    $lang = config('client.lang');
    return Airport::getFullName($lang, $this->originCode);
  }

  public function destinName() {
    $lang = config('client.lang');
    return Airport::getFullName($lang, $this->destinCode);
  }

  protected function validationData() {
    $fields = array_merge(
      get_object_vars($this), 
      [
        'departure'    => method_exists($this->departure, 'format') ? $this->departure->format(DATE_FORMAT_DE) : null,
        'numPaxes'     => intval($this->numAdt) + intval($this->numChd),
        'originDestin' => [ $this->originCode, $this->destinCode] 
      ]
    );

    // Handle conditional return date
    if (method_exists($this->return, 'format')) {
      $fields['return'] = $this->return->format(DATE_FORMAT_DE);
    } else {
      unset($fields['return']);
    }

    return $fields;
  }

  protected function rules() {
    return array_merge(
      parent::rules(),
      [
        'numAdt'      => 'integer|between:'.MINADT.','.MAXADT,
        'numChd'      => 'integer|between:'.MINCHD.','.MAXCHD,
        'numInf'      => 'integer|between:'.MININF.','.MAXINF.'|twoe_less_equal_field:numAdt',
        // 'numJourneys' => 'integer|between:1,2',
        // 'originCode'  => 'bail|required|alpha|size:3|exists:cat_airport,airport_code',
        // 'destinCode'  => 'bail|required|alpha|size:3|different:originCode|exists:cat_airport,airport_code',
        // 'railFly'     => 'boolean',
        'departure'   => 'bail|required|date_format:'.DATE_FORMAT_DE.'|twoe_departure:return',
        'return'      => 'bail|date_format:'.DATE_FORMAT_DE.'|twoe_return:departure',

        // This fields are created to validate groups
        'numPaxes'     => 'required|integer|max:'.MAXPAX,
        // 'originDestin' => 'required|array|twoe_route_exists',
      ]
    );
  }

  /**
   * Define error messages
   * @return array Map of messages to display per validation
   */
  protected function messages() {
    $lang = config('client.lang');
    return [
      'numInf.twoe_less_equal_field'   => ScreenText::get($lang, 'error', 'NUMINF_INVALID'),
      'originCode.*'                   => ScreenText::get($lang, 'error', 'BADAIRPORT'),
      'destinCode.different'           => ScreenText::get($lang, 'error', 'DESTIN_IS_ORIGIN'),
      'destinCode.exists'              => ScreenText::get($lang, 'error', 'BADAIRPORT'),
      'destinCode.*'                   => ScreenText::get($lang, 'error', 'BADAIRPORT'),
      'departure.date_format'          => ScreenText::get($lang, 'error', 'DATE_DEPARTURE_INVALID'),
      'departure.different'            => ScreenText::get($lang, 'error', 'DATE_DEPARTURE_INVALID'),
      'departure.twoe_departure'       => ScreenText::get($lang, 'error', 'DATE_DEPARTURE_INVALID'),
      'return.date_format'             => ScreenText::get($lang, 'error', 'DATE_RETURN_INVALID'),
      'return.different'               => ScreenText::get($lang, 'error', 'DATE_RETURN_INVALID'),
      'return.twoe_return'             => ScreenText::get($lang, 'error', 'DATE_RETURN_INVALID'),
      'numPaxes.max'                   => ScreenText::get($lang, 'error', 'NUMPAX_MAX'),
      'originDestin.twoe_route_exists' => ScreenText::get($lang, 'error', 'NO_ROUTE'),
    ];
  }

  /**
   * Serializes to Json (Hash Array)
   * @return array
   */
  public function jsonSerialize()
  {
    return [
      'numAdt'      => intval($this->numAdt),
      'numChd'      => intval($this->numChd),
      'numInf'      => intval($this->numInf),
      // 'numJourneys' => intval($this->numJourneys),
      'originName'  => $this->originName(),
      'destinName'  => $this->destinName(),
      'originCode'  => $this->originCode,
      'destinCode'  => $this->destinCode,
      'railFly'     => $this->railFly == true,
      'departure'   => method_exists($this->departure, 'format') ? $this->departure->format(DATE_FORMAT_DE) : null,
      'return'      => method_exists($this->return,    'format') ? $this->return->format(DATE_FORMAT_DE)    : null,
    ];
  }
}
