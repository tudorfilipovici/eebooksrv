<?php
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi;

/**
 * Stores data for a PRICE record and the associated CHARGE, PENALTY and CHANGEFEE records
 */
class Price extends EdifactModel {
  public $id            = '';
  public $journey       = '';
  public $compartment   = '';
  public $fareBasisAdt  = '';
  public $fareBasisChd  = '';
  public $fareBasisInf  = '';
  public $fareAmountAdt = '';
  public $fareAmountChd = '';
  public $fareAmountInf = '';
  public $currency      = '';
  public $fareClass     = '';
  public $bookingClass  = '';     // String with the booking classes per leg
  public $eVoucher      = '';
  // TODO: Move this to a client specific class
  public $info          = '';
  public $textRef       = '';
  public $fareRef       = '';
  public $taxIndicator  = '';
  // public $multipleFares = '';
  // public $webFare       = '';
  // public $webFareBasisAdt  = '';
  // public $webFareBasisChd  = '';
  // public $webFareAmountAdt = 0;
  // public $webFareAmountChd = 0;

  /**
   * Serialize to Edifact
   * @return array  PRICE record
   */
  public function toEdifact()
  {
    return [
      'PRICE',
      $this->id,
      $this->journey,
      $this->compartment,
      $this->fareBasisAdt,
      $this->fareAmountAdt,
      $this->fareBasisChd,
      $this->fareAmountChd,
      $this->fareBasisInf,
      $this->fareAmountInf,
      $this->currency,
      $this->taxIndicator,
      $this->fareClass,
      $this->bookingClass,
      $this->eVoucher,
      $this->info,
      $this->textRef,
      $this->fareRef,
      // $this->multipleFares,
      // $this->webFare,
      // $this->webFareBasisAdt,
      // $this->webFareAmountAdt,
      // $this->webFareBasisChd,
      // $this->webFareAmountChd,
      '',
    ];
  }

  /**
   * parse Edifact PRICE record
   * @param  array  $fields PRICE record
   * @return Price          Reference to this
   */
  public function parseEdifact(array $fields)
  {
    $fields = array_pad($fields, 18, '');
    $this->id               = $fields[1];
    $this->journey          = $fields[2];
    $this->compartment      = $fields[3];
    $this->fareBasisAdt     = $fields[4];
    $this->fareAmountAdt    = $this->parseFloat($fields[5]);
    $this->fareBasisChd     = $fields[6];
    $this->fareAmountChd    = $this->parseFloat($fields[7]);
    $this->fareBasisInf     = $fields[8];
    $this->fareAmountInf    = $this->parseFloat($fields[9]);
    $this->currency         = $fields[10];
    $this->taxIndicator     = $fields[11];
    $this->fareClass        = $fields[12];
    $this->bookingClass     = $fields[13];
    $this->eVoucher         = $fields[14];
    $this->info             = $fields[15];
    $this->textRef          = $fields[16];
    $this->fareRef          = $fields[17];
    // $this->multipleFares    = $fields[18];
    // $this->webFare          = $fields[19];
    // $this->webFareBasisAdt  = $fields[20];
    // $this->webFareAmountAdt = floatval($fields[21]);
    // $this->webFareBasisChd  = $fields[22];
    // $this->webFareAmountChd = floatval($fields[23]);
    return $this;
  }

  public function getFareBasis($paxType) 
  {
    switch ($paxType) {
      case 'ADT':
        return $this->fareBasisAdt;

      case 'CHD':
        return $this->fareBasisChd;

      case 'INF':
        return $this->fareBasisInf;
    }
    return '';
  }
}
/** @} */