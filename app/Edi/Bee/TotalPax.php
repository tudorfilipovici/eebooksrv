<?php 
/**
 * \defgroup UTIL_PRICE_BREAKDOWN Price Breakdown
 * Common Edifact Records uses for price breakdowns
 * @{
 */

namespace App\Edi\Bee;

use App\Edi\TotalPax as TwoeTotalPax;

class TotalPax extends TwoeTotalPax {
  protected $paxTotal  = 0;
  protected $paxFare   = 0;
  protected $paxTaxes  = 0;
  protected $paxTSC    = 0;

  public function toEdifact()
  {
    return [
      'TOTALPAX',
      $this->paxType,
      $this->paxNum,
      $this->totalPrice,
      $this->totalFare,
      $this->totalTaxes,
      $this->totalTSC, // total fees
      $this->paxTotal, // total per single pax
      $this->paxFare,  // fares per single pax
      $this->paxTaxes, // taxes per single pax
      $this->paxTSC,   // fees  per single pax
      $this->discount,
      $this->currency,
      '',
    ];
  }

  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->paxType     = next($fields);
    $this->paxNum      = $this->parseInt(next($fields), 0);
    $this->totalPrice  = $this->parseFloat(next($fields));
    $this->totalFare   = $this->parseFloat(next($fields));
    $this->totalTaxes  = $this->parseFloat(next($fields));
    $this->totalTSC    = $this->parseFloat(next($fields));
    $this->paxTotal    = $this->parseFloat(next($fields));
    $this->paxFare     = $this->parseFloat(next($fields));
    $this->paxTaxes    = $this->parseFloat(next($fields));
    $this->paxTSC      = $this->parseFloat(next($fields));
    $this->discount    = $this->parseFloat(next($fields));
    $this->currency    = next($fields);
    return $this;
  }
}

/** @} */
