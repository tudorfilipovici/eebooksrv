<?php 
namespace App\Edi\Bee;

use App\Edi\Req as TwoeReq;
use DateTime;

class Req extends TwoeReq { 
  /**
   *
   */
  public function toEdifact() {
    $depDate = $this->departure ? $this->departure->format(DATE_FORMAT_DE) : '';
    $retDate = $this->return    ? $this->return->format(DATE_FORMAT_DE)    : '';
    return [
      'REQ',
      $this->originCode,
      $this->destinCode,
      $depDate,               // departure date
      $retDate,               // return date
      'D',                    // time type
      $depDate ? '0001' : '', // departure time
      $retDate ? '0001' : '', // return time
      '',                     // carrier
      'N',                    // direct only
      '',                     // class
      'LC',                   // bias
      $this->numPax,          // num pax
      $this->numChd,          // num chd
      $this->numInf,          // num inf
      '',                     // compartment
      'N',                    // unifares
      '',                     // corp net rates
      'N',                    // inbound
      'GBP',                  // currency
      'N',                    // sector priced 
      'Y',                    // use cache
      'Y',                    // use host
      'Y',                    // tabs required
      ''                      // endOfRecord
    ];
  }
};
