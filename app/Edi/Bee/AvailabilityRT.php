<?php
namespace App\Edi\Bee;

use Illuminate\Support\Facades\Log;
use DateTimeInterface;
use DateTime;
use DateTimeZone;
use DateInterval;

use App\Edi\Availability as TwoeAvailability;
use App\Edi\MatrixItem;
use App\Edi\TaxElement;

/**
 * Stores the data from a CHARGE record
 */
class AvailabilityRT extends TwoeAvailability 
{
  const METHOD_REQUEST = 'PRAVLREQ';

  public function __construct() {
    parent::__construct();
    $this->factory->register([
      'REQ'       => Req::class,
      'FLTHEADER' => FltHeader::class,
      'CONN'      => FlightLeg::class,
      'FLT'       => FlightLeg::class,
      'CLASS'     => FltClass::class,
      'PRICE'     => Price::class,
      'TABPRICE'  => TabPrice::class,
      // 'TAX'       => Tax::class,
      'TAXELE'    => TaxElement::class,
      'TOTAL'     => Total::class,
      'TOTALPAX'  => TotalPax::class,
      // 'MXELE'     => MatrixItem::class,
    ]);
  }

  /**
   * Parses a MATRIX row record
   * @param  array $record The MATRIX record with some columns
   */
  protected function parseMATRIX(array $record, array &$data) 
  {
    $data['MATRIX'] = $record;
  }

  /**
   * parses TAXELE record
   */
  protected function parseTAXELE(array $record, array &$data) {
    $tax = $data['TAX'];
    $tax->addTaxElt($this->factory->parse('TAXELE', $record));
  }

  /**
   * [parseMXELE description]
   * @param  array  $record [description]
   * @param  array  &$data  [description]
   * @return [type]         [description]
   */
  protected function parseMXELE(array $record, array &$data) 
  {
    reset($data['MATRIX']);
    $fix1  = next($data['MATRIX']);
    $fix2  = next($data['MATRIX']);
    $comp1 = next($data['MATRIX']);

    reset($record);
    $comp2 = next($record);
    $info  = next($record);
    $tot   = next($record);

    $p1 = $p2 = $tax = $fee = '';
    if (isset($this->totals[$tot])) {
      $p1  = $this->totals[$tot]->fareIdOut;
      $p2  = $this->totals[$tot]->fareIdIn;
      $tax = $this->totals[$tot]->taxId;
      $fee = $this->totals[$tot]->feeId;
    }

    $itm = $this->parseMatrixCell($fix1, $fix2, $comp1, $comp2, [$info, $p1, $p2, $tax, $fee, $tot]);
    // Assign code "0-X" for empty inbounds
    if( empty( $fix2)) {
      $fix2  = '0';
      $comp2 = 'X';
    }
    $this->addMatrixItem($fix1, $fix2, $comp1, $comp2, $itm);
  }

  protected function parseMatrixCell($fix1, $fix2, $comp1, $comp2, array $record)
  {
    list($info, $p1, $p2, $tax, $fee, $tot) = $record;

    $item = new MatrixItem([
      'info'      => $info,
      'fltRefOut' => $fix1,
      'fltRefIn'  => $fix2,
      'compOut'   => $comp1,
      'compIn'    => $comp2,
      'flightOut' => $this->getFlight(OUTBOUND, $fix1) ?: null,
      'flightIn'  => $this->getFlight(INBOUND,  $fix2) ?: null,
      'fareOut'   => isset( $this->prices[$p1])  ? $this->prices[$p1]  : null,
      'fareIn'    => isset( $this->prices[$p2])  ? $this->prices[$p2]  : null,
      'total'     => isset( $this->totals[$tot]) ? $this->totals[$tot] : null,
      'tax'       => isset( $this->taxes[$tax])  ? $this->taxes[$tax]  : null,
    ]);

    return $item;
  }
}
