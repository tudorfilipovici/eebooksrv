<?php
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi\Bee;

use App\Edi\Price as TwoePrice;

/**
 * Stores data for a PRICE record and the associated CHARGE, PENALTY and CHANGEFEE records
 */
class Price extends TwoePrice {
  protected $eVoucherDiscount = '';
  protected $fareType         = '';
  protected $corpNetRate      = '';
  protected $webfareId        = '';
  protected $webDiscountAdt   = '';
  protected $webDiscountChd   = '';
  protected $webDiscountInf   = '';
  protected $origFareAdt      = '';
  protected $origFareChd      = '';
  protected $origFareInf      = '';

  /**
   * Serialize to Edifact
   * @return array  PRICE record
   */
  public function toEdifact()
  {
    return [
      'PRICE',
      $this->id,
      $this->compartment,
      $this->journey,
      $this->fareBasisAdt,
      $this->fareAmountAdt,
      $this->fareBasisChd,
      $this->fareAmountChd,
      $this->fareBasisInf,
      $this->fareAmountInf,
      $this->currency,
      $this->fareClass,
      $this->bookingClass,
      $this->eVoucherDiscount,
      $this->eVoucher,
      $this->fareType,
      $this->corpNetRate,
      $this->webfareId,
      $this->webDiscountAdt,
      $this->webDiscountChd,
      $this->webDiscountInf,
      $this->origFareAdt,
      $this->origFareChd,
      $this->origFareInf,
      '',
    ];
  }

  /**
   * parse Edifact PRICE record
   * @param  array  $fields PRICE record
   * @return Price          Reference to this
   */
  public function parseEdifact(array $fields)
  {
    $this->id               = next($fields);
    $this->compartment      = next($fields);
    $this->journey          = next($fields);
    $this->fareBasisAdt     = next($fields);
    $this->fareAmountAdt    = $this->parseFloat(next($fields));
    $this->fareBasisChd     = next($fields);;
    $this->fareAmountChd    = $this->parseFloat(next($fields));
    $this->fareBasisInf     = next($fields);
    $this->fareAmountInf    = $this->parseFloat(next($fields));
    $this->currency         = next($fields);
    $this->fareClass        = next($fields);
    $this->bookingClass     = next($fields);
    $this->eVoucherDiscount = next($fields);
    $this->eVoucher         = next($fields);
    $this->fareType         = next($fields);
    // The following are not really used...
    $this->corpNetRate      = next($fields);
    $this->webfareId        = next($fields);
    $this->webDiscountAdt   = next($fields);
    $this->webDiscountChd   = next($fields);
    $this->webDiscountInf   = next($fields);
    $this->origFareAdt      = next($fields);
    $this->origFareChd      = next($fields);
    $this->origFareInf      = next($fields);

    return $this;
  }

  public function getWebDiscount($paxType) 
  {
    switch ($paxType) {
      case 'ADT':
        return $this->webDiscountAdt;

      case 'CHD':
        return $this->webDiscountChd;

      case 'INF':
        return $this->webDiscountInf;
    }
    return '';
  }

  public function getOrigFare($paxType) 
  {
    switch ($paxType) {
      case 'ADT':
        return $this->origFareAdt;

      case 'CHD':
        return $this->origFareChd;

      case 'INF':
        return $this->origFareInf;
    }
    return '';
  }
}
/** @} */
