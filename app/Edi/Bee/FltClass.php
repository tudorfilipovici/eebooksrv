<?php 
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi\Bee;
use App\Edi\FltClass as TwoeFltClass;

/**
 * Stores data from a CLASS record which is related to a FLT or CONN records
 */
class FltClass extends TwoeFltClass {
  protected $totalRef = '';

  public function toEdifact()
  {
    return [
      'CLASS',
      $this->compartment,
      $this->bookingClass,
      $this->numAvailSeats,
      $this->totalRef,
      '',
    ];
  }

  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->compartment   = next($fields);
    $this->bookingClass  = next($fields);
    $this->numAvailSeats = intval(next($fields));
    $this->totalRef      = next($fields);
    return $this;
  }
}

/** @} */