<?php 
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi\Bee;

use DateTime;
use App\Edi\TabPrice as TwoeTabPrice;

/**
 * Stores the data from a CHARGE record
 */
class TabPrice extends TwoeTabPrice {
  const INFO_UNKNOWN  = 'UNKNOWN';   // No information in cache

  /**
   * Serializes to Edifact array
   * @return array Array of values
   */
  public function toEdifact()
  {
    return [
      'TABPRICE',
      $this->isOutbound ? self::DIRECTION_OUT : self::DIRECTION_IN,
      $this->date->format(DATE_FORMAT_DE),
      $this->info,
      $this->currency,
      $this->amount,
      $this->compartment,
      '',
    ];
  }

  /**
   * Loads data from an Edifact array
   * @param  array  $fields The TABPRICE record
   * @return \Edi\TabPrice  Reference to this
   */
  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->isOutbound  = next($fields) == self::DIRECTION_OUT;
    $this->date        = $this->parseDate(next($fields), DATE_FORMAT_DE);
    $this->info        = next($fields);
    $this->currency    = next($fields);
    $this->amount      = $this->parseFloat(next($fields));
    $this->compartment = next($fields);

    // Reset time
    $this->resetTime();
    return $this;
  }
}
/** @} */