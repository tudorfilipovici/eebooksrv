<?php 
/**
 * \defgroup UTIL_PRICE_BREAKDOWN Price Breakdown
 * Common Edifact Records uses for price breakdowns
 * @{
 */

namespace App\Edi\Bee;

use App\Edi\Total as TwoeTotal;

/**
 * Contains data from a TOTAL segment and the related TOTALPAX details per adt, chd and inf
 */
class Total extends TwoeTotal {
  protected $compOut   = '';
  protected $compIn    = '';
  protected $feeTotal  = '';
  protected $feeId     = '';
  protected $fareIdOut = '';
  protected $fareIdIn  = '';
  protected $taxId     = '';

  /**
   * Returns a TOTAL array segment
   * @return array TOTAL segment
   * 
   * TOTAL~1~BF~BF~1~1~A~~584.08~0.00~~GBP~|
   */
  public function toEdifact()
  {
    return [
      'TOTAL',
      $this->id,
      $this->compOut,
      $this->compIn,
      $this->fareIdOut,
      $this->fareIdIn,
      $this->taxId,
      $this->feeId,
      $this->amount,
      $this->feeTotal,
      $this->discount,
      $this->currency,
      '',
    ];
  }

  /**
   * Returns total for the selected pax type
   * @param  string $paxType Pax type: ['ADT', 'CHD', 'INF']
   * @return TotalPax        The total per pax or null if paxType is invalid
   */
  public function getTotalPax($paxType) 
  {
    switch ($paxType) {
      case 'ADT':
        return $this->totalAdt;
      case 'CHD':
        return $this->totalChd;
      case 'INF':
        return $this->totalInf;
    }
    return null;
  }

  /**
   * Parses a PAXTOTAL array segment into a PaxTotal Instance
   * @param  array  $fields PAXTOTAL segment
   * @return TotalPax       The total per pax or null if paxType is invalid
   */
  public function parseTotalPax(array $fields)
  {
    $total = TotalPax::create($fields);
    switch ($total->paxType) {
      case 'ADT':
        $this->totalAdt = $total;
        break;
      case 'CHD':
        $this->totalChd = $total;
        break;
      case 'INF':
        $this->totalInf = $total;
        break;
    }
    return $total;
  }

  /**
   * Parses a TOTAL segment
   * @param  array  $fields TOTAL segment
   * @return Total          Reference to $this
   */
  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->id        = next($fields);
    $this->compOut   = next($fields);
    $this->compIn    = next($fields);
    $this->fareIdOut = next($fields);
    $this->fareIdIn  = next($fields);    
    $this->taxId     = next($fields);
    $this->feeId     = next($fields);    
    $this->amount    = $this->parseFloat(next($fields), 0);
    $this->feeTotal  = $this->parseFloat(next($fields));
    $this->discount  = $this->parseFloat(next($fields));
    $this->currency  = next($fields);
    return $this;
  }
}
/** @} */