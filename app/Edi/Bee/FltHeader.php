<?php 
namespace App\Edi\Bee;

use App\Edi\FltHeader as TwoeFltHeader;
use DateTime;

/**
 * Stores the data from a FLTHEADER record
 */
class FltHeader extends TwoeFltHeader {

  protected $timestamp   = '';
  protected $negoFares   = '';
  protected $isApisRoute = false;

  public function toEdifact()
  {
    return [
      'FLTHEADER',
      $this->journey === 1 ? 'OUT' : 'IN',
      $this->date->format(DATE_FORMAT_DE),
      $this->status,
      $this->timestamp,
      $this->negoFares,
      $this->isApisRoute ? 'Y' : 'N',
      '',
    ];
  }

  public function parseEdifact(array $fields)
  {
    $fields = array_pad($fields, 7, '');
    $this->journey   = $fields[1] === 'OUT' ? 1 : 2;
    $this->date      = $this->parseDate($fields[2], DATE_FORMAT_DE, new DateTime());
    $this->status    = $fields[3];

    $this->timestamp = $fields[4];
    $this->negoFares = $fields[5];
    $this->apisRoute = $fields[6] == 'Y';

    // Move date times
    $this->resetTime();
    return $this;
  }
}
