<?php 
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi\Bee;

use DateTime;
use DateTimeZone;
use App\Utils\DateTimeUtil;
use App\Edi\FlightLeg as TwoeFlightLeg;

/**
 * Stores data from a FLT or CONN record
 */
class FlightLeg extends TwoeFlightLeg {
  /**
   * Loads the values from an edifact FLT or CONN array of values
   * @param  array  $fields Array of values
   * @return FlightLeg      Reference to this instance
   */
  public function parseEdifact(array $fields)
  {
    $this->isConnection  = (reset($fields) == 'CONN');
    $this->fltRef        = next($fields);
    $this->carrierCode   = next($fields);
    $this->operatorCode  = next($fields);
    $this->isCodeShare   = next($fields) == 'Y';
    $this->providerName  = next($fields);
    $this->fltNumber     = next($fields);
    $this->originCode    = next($fields);
    $this->destinCode    = next($fields);
    $this->departure     = $this->parseAirportDate($this->originCode, next($fields).' '.next($fields), DATETIME_FORMAT_INTERNAL);
    $this->arrival       = $this->parseAirportDate($this->destinCode, next($fields).' '.next($fields), DATETIME_FORMAT_INTERNAL);
    $this->numStops      = intval(next($fields));
    $this->depTerminal   = next($fields);
    $this->arrTerminal   = next($fields);
    $this->equipmentCode = next($fields);
    $this->duration      = $this->parseDuration(next($fields));
    $this->isETicket     = next($fields) == 'Y';
    $this->opChangeInd   = next($fields);
    return $this;
  }

  /**
   * Serializes back to Edifact
   * @return array The CONN or FLT array of values
   */
  public function toEdifact()
  {
    return [
      $this->isConnection ? 'CONN' : 'FLT',
      $this->fltRef,
      $this->carrierCode,
      $this->operatorCode,
      $this->isCodeShare ? 'Y' : 'N',
      $this->providerName,
      $this->number,
      $this->originCode,
      $this->destinCode,
      $this->departure->format(DATE_FORMAT_DE),
      $this->departure->format('Hi'),
      $this->arrival->format(DATE_FORMAT_DE),
      $this->arrival->format('Hi'),
      $this->numStops,
      $this->depTerminal,
      $this->arrTerminal,
      $this->equipmentCode,
      implode('', $this->durationHrsMins()),
      $this->isETicket ? 'Y' : 'N',
      $this->opChangeInd,
      ''
    ];
  }
}
/** @} */
