<?php
namespace App\Edi;

use DateTimeZone;
use DateTime;
use App\Utils\DateTimeUtil;

trait ParsesPriceMatrixTrait {
 
  // Combination matrix
  protected $matrix = [];
  // List of all the matrix items
  protected $matrixItems = [];

  /**
   * Parses a MATRIX row record
   * @param  array $record The MATRIX record with some columns
   */
  protected function parseMATRIX(array $record, array &$data) {
    $idx   = 0;
    $len   = count($record);
    $flts  = $idx < $len ? $record[++$idx] : ''; // get outbound/inbound flight refs
    $comp1 = $idx < $len ? $record[++$idx] : ''; // get the outbound compartment

    // get each flight ref
    list( $fix1, $fix2) = array_pad(explode(',', $flts), 2, '');

    // Iterate the row per each compartment
    while ($idx < $len && ($comp2 = $record[++$idx])) {
      // Fill the rest of columns
      $fields = [];
      for( $i = 1; $idx < $len && $i < static::MATRIX_NUM_FIELDS; ++$i) {
        $fields[] = $record[++$idx];
      }
      // Parse the rest of fields for this cell
      $itm = $this->parseMatrixCell($fix1, $fix2, $comp1, $comp2, $fields);
      // Assign code "0-X" for empty inbounds
      if( empty( $fix2)) {
        $fix2  = '0';
        $comp2 = 'X';
      }
      $this->addMatrixItem($fix1, $fix2, $comp1, $comp2, $itm);
    }
  }

  /**
   * addMatrixItem
   * 
   * @param  number $fix1     Outbound flight index
   * @param  number $fix2     Inbound flight index
   * @param  string $comp1    Outbound compartment code
   * @param  string $comp2    Inbound compartment code
   * @param  MatrixItem $itm  EdifactModel Instance
   */
  protected function addMatrixItem($fix1, $fix2, $comp1, $comp2, $itm) {
    // Store the result in the matrix
    $this->matrix["$fix1-$comp1"]["$fix2-$comp2"] = $itm;
    // Store a plain list of the matrix items
    $this->matrixItems[] = $itm;
  }

  // protected function parseMatrixCell($fix1, $fix2, $comp1, $comp2, $fields) {}
}
