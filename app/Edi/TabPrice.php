<?php 
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi;

use DateTime;

/**
 * Stores the data from a CHARGE record
 */
class TabPrice extends EdifactModel { // implements \JsonSerializable {
  const INFO_NOFLIGHTS = 'NOFLIGHTS'; // No flights on this date
  const INFO_FLIGHTS   = 'FLIGHTS';   // Has available fares
  const INFO_SOLDOUT   = 'SOLDOUT';   // All fares are soldout
  const INFO_DISABLED  = 'DISABLED';  // A date with price info, but can't be selected
  const INFO_INVALID   = 'INVALID';   // Cannot select this date (missing info or in the past)

  const DIRECTION_OUT = 'OUT';
  const DIRECTION_IN  = 'IN';

  protected $isOutbound  = true;
  protected $date        = '';
  protected $info        = '';
  protected $amount      = '';
  protected $currency    = '';
  protected $compartment = '';

  protected $active   = false;

  /**
   * @return boolean Returns true if the instance has valid price info
   */
  public function hasPrice() 
  {
    return (!empty($this->amount) && $this->amount > 0);
  }

  /**
   * Sets the state as active
   * @param boolean $val The active state to set
   */
  public function setActive($val) {
    $this->active = ($val == true);
  }

  /**
   * @return boolean True if this tab is active
   */
  public function isActive() {
    return $this->active;
  }

  /**
   * Serializes to Json (Hash Array)
   * @return array
   */
  // public function jsonSerialize()
  // {
  //   return [
  //     'date'     => $this->date->format(DATE_FORMAT_DE),
  //     'active'   => $this->active,
  //     'info'     => $this->info,
  //     'amount'   => intval($this->amount),
  //     'currency' => $this->currency,
  //   ];
  // }

  /**
   * [setTimezone description]
   * @param Req $req [description]
   */
  public function setDate(DateTime $date) {
    $this->date = $date;
  }

  public function getDate($format = null) {
    return $format ? $this->date->format($format) : (clone $this->date);
  }

  protected function resetTime() 
  {
    if ($this->isOutbound) {
      $this->date->setTime(0, 0, 0);
    } else {
      $this->date->setTime(23, 59, 59);
    }    
  }

  /**
   * Serializes to Edifact array
   * @return array Array of values
   */
  public function toEdifact()
  {
    return [
      'TABPRICE',
      $this->isOutbound ? self::DIRECTION_OUT : self::DIRECTION_IN,
      $this->date->format(DATE_FORMAT_DE),
      $this->info,
      $this->amount,
      $this->currency,
      $this->compartment,
      '',
    ];
  }

  /**
   * Loads data from an Edifact array
   * @param  array  $fields The TABPRICE record
   * @return \Edi\TabPrice  Reference to this
   */
  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->isOutbound  = next($fields) == self::DIRECTION_OUT;
    $this->date        = $this->parseDate(next($fields), DATE_FORMAT_DE);
    $this->info        = next($fields);
    $this->amount      = $this->parseFloat(next($fields));
    $this->currency    = next($fields);
    $this->compartment = next($fields);

    // Reset time
    $this->resetTime();
    return $this;
  }
}
/** @} */