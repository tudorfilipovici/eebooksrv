<?php 
namespace App\Edi;
/**
  @brief Fill with patterns to filter credit card and password information.
 */
class PatternFiller {
  private $userSearch     = "#(\|USER~[^~]*~)([^~]*)(~)([^~]*)(~[^~]*~)([^~]*~)([^~]*)#i";
  private $cardSearch     = "#(\|CARD~[^~]*~[^~]*~)([^~]*)(~[^~]*~)([^~]*)#i";
  private $prffcardSearch = "#(\|PRFFCARD~[^~]*~)([^~]*)~([^~]*)~#i";
  private $apisDocsSearch = "#(\|APISDOCS~[^~]*~[^~]*~)([0-9]{5,})(~[^~]*~[^~]*~[^~]*~[^~]*~)([0-9]{5,})([^~]*~[^~]*~[^~]*~[^~]*~[^~]*~[^~]*~[^~]*~)#i";

  public function replaceUser($value) {
    return preg_replace_callback($this->userSearch, function($m) {
      return $m[1]. str_repeat( '*', strlen( $m[2])) .$m[3]. str_repeat( '*', strlen( $m[4])) .$m[5]. str_repeat( '*', strlen( $m[6]));
    }, $value);
  }

  public function replaceCard($value) {
    return preg_replace_callback($this->cardSearch, function($m) {
      return $m[1]. str_repeat( '*', strlen( $m[2]) - 4) .substr( $m[2], -4) .$m[3]. str_repeat( '*', strlen( $m[4]));
    }, $value);
  }

  public function replacePrffCard($value) {
    return preg_replace_callback($this->prffcardSearch, function($m) {
      return $m[1]. str_repeat( '*', strlen( $m[2]) - 4) .substr( $m[2], -4) .'~'. str_repeat( '*', strlen( $m[3]));
    }, $value);
  }

  public function replaceApisDocs($value) {
    return preg_replace_callback($this->apisDocsSearch, function($m) {
      return $m[1]. @str_repeat( '*',strlen( $m[2]) - 4). substr( $m[2], -4) .$m[3].str_repeat( '*', strlen( $m[4]));
    }, $value);
  }

  public function replace($value) {
    $value = $this->replaceUser($value);
    $value = $this->replaceCard($value);
    $value = $this->replacePrffCard($value);
    $value = $this->replaceApisDocs($value);
    return $value;
  }
}