<?php
namespace App\Edi;

use Illuminate\Support\Facades\Log;
use DateTimeInterface;
use DateTime;
use DateTimeZone;
use DateInterval;

use App\Utils\DateTimeUtil;
use App\Models\ScreenText;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exception\HttpResponseException;

/**
 * Stores the data from a CHARGE record
 */
class Availability extends MiddlewareRequest {
  use ParsesFlightsTrait;
  use ParsesTabPricesTrait;
  use ParsesPriceBreakdownTrait;
  use ParsesPriceMatrixTrait;

  protected $req       = null;

  // RAW records
  protected $tktMethod = [];
  // Response type
  protected $type      = '';

  // Extra info
  protected $id       = '';
  protected $currency = '';

  protected $selected = null;       // selected matrix item

  const METHOD_REQUEST    = 'AVAILREQ';
  const MATRIX_NUM_FIELDS = 5;   // Seven fields per column
  const REQUEST_TIMEOUT   = 150;

  public function __construct() {
    $this->factory = new RecordsFactory([
      'REQ'       => Req::class,
      'FLTHEADER' => FltHeader::class,
      'CONN'      => FlightLeg::class,
      'FLT'       => FlightLeg::class,
      'CLASS'     => FltClass::class,
      'MATRIX'    => MatrixItem::class,
      'PRICE'     => Price::class,
      'TABPRICE'  => TabPrice::class,
      'TAX'       => Tax::class,
      'TAXELT'    => TaxElement::class,
      'TOTAL'     => Total::class,
      'TOTALPAX'  => TotalPax::class,
      'MATRIX'    => MatrixItem::class,
    ]);
  }

  /**
   * Sends a request to MW
   * @param  FlightSearch $search The search settings
   */
  public function request(array $search) 
  {
    $this->req = $this->factory->create('REQ', $search);
    list($status, $resp) = $this->sendMessage(static::METHOD_REQUEST, $this->getRequestData(), static::REQUEST_TIMEOUT);

    // check the result
    if($status != 'SUCCESS') {
      Log::error(static::METHOD_REQUEST, [$status]);
      throw new HttpResponseException($this->getErrorResponse($status));
    }

    $this->parseEdifact($resp);
  }

  /**
   * Data sent to AVAILREQ
   * @return array
   */
  public function getRequestData()  {
    return [ $this->req->toEdifact() ];
  }

  /**
   * [getErrorResponse description]
   * @param  string $status   Error code
   * @return JsonResponse     Json Response with error code and description
   */
  protected function getErrorResponse($status) {
    $error = [
      'error' => ScreenText::get(config('client.lang'), 'error', $status)
    ];
    return new JsonResponse($error, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
  }

  /**
   * parse Edifact AVAILRES response
   * @param  array  $records Array of records
   */
  public function parseEdifact(array $records)
  {
    $data = [];
    $this->initFlightsParser($data,  $this->req);
    $this->initTabPriceParser($data, $this->req);

    foreach ($records as $record) {
      $recordName = $record[0];
      if( method_exists( $this, "parse{$recordName}")) {
        call_user_func_array([$this, "parse{$recordName}"], [$record, &$data]);
      }
    }

    // Take the currency from the last price
    if( isset( $data['PRICE'])) {
      $this->currency = $data['PRICE']->currency;
    }
  }

  /**
   * Parse a TKTMETHOD record
   * @param  array  $record The array of values
   */
  protected function parseTKTMETHOD(array $record) {
    $this->tktMethod[$record[1]] = $record; // TKTMETHOD_TICKETING
  }

  /**
   * Parse a RESPTYPE record
   * @param  array  $record The array of values
   */
  protected function parseRESPTYPE(array $record) {
    $this->type = $record[1];
  }

  /**
   * Parse a REQ record
   * @param  array  $record The array of values
   */
  protected function parseREQ(array $record, array $data) {
    $this->req = $this->factory->parse('REQ', $record);
  }

  /**
   * Searches for a flight in the journeys array, using given jix and fix
   * @param  number $jix Journey index
   * @param  number $fix Flight index
   * @return \Availability\Flight   The flight set instance or null if none match.
   */
  protected function getFlight($jix, $fix)
  {
    if (empty($fix) || !isset($this->journeys[$jix]->flights[$fix])) {
      return null;
    }
    return $this->journeys[$jix]->flights[$fix];
  }

  /**
   * Parses the contents for a given combination in the MATRIX record
   *
   * @param  number $fix1   Outbound flight index
   * @param  number $fix2   Inbound flight index
   * @param  string $comp1  Outbound compartment code
   * @param  string $comp2  Inbound compartment code
   * @param  array  $record Array of field values
   *
   * @return MatrixItem     The instance
   */
  protected function parseMatrixCell($fix1, $fix2, $comp1, $comp2, array $record)
  {
    list($info, $prices, $tax, $tot) = $record;
    list($p1, $p2) = array_pad( explode(',', $prices), 2, '');

    $item = new MatrixItem([
      'info'      => $info,
      'fltRefOut' => $fix1,
      'fltRefIn'  => $fix2,
      'compOut'   => $comp1,
      'compIn'    => $comp2,
      'flightOut' => $this->getFlight(OUTBOUND, $fix1) ?: null,
      'flightIn'  => $this->getFlight(INBOUND,  $fix2) ?: null,
      'fareOut'   => isset( $this->prices[$p1])  ? $this->prices[$p1]  : null,
      'fareIn'    => isset( $this->prices[$p2])  ? $this->prices[$p2]  : null,
      'total'     => isset( $this->totals[$tot]) ? $this->totals[$tot] : null,
      'tax'       => isset( $this->taxes[$tax])  ? $this->taxes[$tax]  : null,
    ]);
    return $item;
  }

  /**
   * @return boolean True if the search was done for one way
   */
  public function isOneWay() {
    return $this->req->isOneWay();
  }

  /**
   * @return boolean True if the search was for round trip
   */
  public function isRoundTrip() {
    return !$this->req->isOneWay();
  }

  /**
   * Given a flight-compartment combination for outbound (and inbound) it
   *  returns the associated matrix item or null if none founc
   * @param  integer $fixOut  Flight index for the outbound
   * @param  string  $compOut Compartment code for outbound
   * @param  integer $fixIn   Flight index for the inbound
   * @param  string  $compIn  Compartment code for inbound
   * @return MatrixItem       The matching price
   */
  public function getPrice($fixOut, $compOut, $fixIn=0, $compIn='X')
  {
    $out = "{$fixOut}-{$compOut}";
    $in  = "{$fixIn}-{$compIn}";
    if (isset($this->matrix[$out][$in])) {
      return $this->matrix[$out][$in];
    }
    return null;
  }

  /**
   * Stores the selected price
   * @param  MatrixItem $price The selected price
   */
  public function selectPrice(MatrixItem $price) {
    $this->selected = $price;
  }

  /**
   * Returns the selected price
   * @return MatrixItem The stored price or null if none.
   */
  public function getSelectedPrice() {
    return $this->selected;
  }

  public function getRequest() {
    $req = clone $this->req;
    return $req;
  }
}
