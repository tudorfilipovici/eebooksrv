<?php
namespace App\Edi;

trait ParsesFlightsTrait {
  protected $journeys  = [];

  /**
   * Initializes the data hash
   * @param  array  $data [description]
   * @return [type]       [description]
   */
  protected function initFlightsParser(array &$data, Req $req) {
    $data['FLTHEADER'] = null;
    $data['FLT']  = null;
    $data['LEGS'] = [];
    $data['FLTHEADER_ORIGIN'] = $req->originCode;
    $data['FLTHEADER_DESTIN'] = $req->destinCode;
  } 

  /**
   * Parse a FLTHEADER record, stores the last instance in data['FLTHEADER']
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parseFLTHEADER(array $record, array &$data) {
    $journey = $this->factory->parse('FLTHEADER', $record);
    $jix     = $journey->isOutbound() ? OUTBOUND : INBOUND;
    $journey->setAirportCode($journey->isOutbound() ? $data['FLTHEADER_ORIGIN'] : $data['FLTHEADER_DESTIN']);
    $this->journeys[$jix] = $data['FLTHEADER'] = $journey;
  }

  /**
   * Parse a CONN record, stores the last instance in data['FLT'], also keeps and array of FlightLegs in data['LEGS']
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parseCONN(array $record, array &$data) {
    $leg = $this->factory->parse('CONN', $record);
    $data['LEGS'][] = $data['FLT'] = $leg;
  }

  /**
   * Parse a FLT record, stores the last instance in data['FLT'],
   * Appends the array of FlightLegs in data['LEGS'] to the last header in $data['FLTHEADER']
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parseFLT(array $record, array &$data) {
    $this->parseCONN($record, $data);
    $journey = $data['FLTHEADER'];
    $journey->addFlightLegs($data['LEGS']);
    $data['LEGS'] = [];
  }

  /**
   * Parse a CLASS record and relates it to the last $data['FLT'] value
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parseCLASS(array $record, array &$data) {
    $flight = $data['FLT'];
    $flight->parseFltClass($record);
  }
}