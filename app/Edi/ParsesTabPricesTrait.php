<?php
namespace App\Edi;

use DateTimeZone;
use DateTime;
use App\Utils\DateTimeUtil;

trait ParsesTabPricesTrait {
  protected $tabPrices = [ OUTBOUND => [], INBOUND => [] ];

  /**
   * Initializes the data hash
   * @param  array  $data [description]
   * @return [type]       [description]
   */
  protected function initTabPriceParser(array &$data, Req $req) {
    $data['TABPRICE_ORIGIN'] = $req->originCode;
    $data['TABPRICE_DESTIN'] = $req->destinCode;
  } 

  /**
   * Parse a TABPRICE record and stores the last instance in $data['PRICE']
   * @param  array  $record The array of values
   */
  protected function parseTABPRICE(array $record, array &$data) {
    $tab    = $this->factory->parse('TABPRICE', $record);
    $jix    = $tab->isOutbound ? OUTBOUND : INBOUND;
    $code   = $tab->isOutbound ? $data['TABPRICE_ORIGIN'] : $data['TABPRICE_DESTIN'];
    $tzName = DateTimeUtil::getTimeZone($code);
    $date   = DateTime::createFromFormat(DATE_FORMAT_DE, $tab->getDate(DATE_FORMAT_DE), new DateTimeZone($tzName));
    $tab->setDate($date);
    $this->tabPrices[$jix][] = $tab;
  }
}