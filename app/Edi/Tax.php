<?php 
/**
 * \defgroup UTIL_PRICE_BREAKDOWN Price Breakdown
 * Common Edifact Records uses for price breakdowns
 * @{
 */

namespace App\Edi;

class Tax extends EdifactModel {
  use PriceItemTrait;
  protected $id        = '';
  protected $direction = ''; // Oneway (OW) or return (RT) tax
  protected $breakdown = [];

  public function addTaxElt($taxElt)
  {
    $this->breakdown[] = $taxElt;
  }

  public function breakdownToEdifact()
  {
    return array_map(function($elt) { return $elt->toEdifact(); }, $this->breakdown);
  }

  public function toEdifact()
  {
    return [
      'TAX',
      $this->id,
      $this->direction,
      $this->adt,
      $this->chd,
      $this->inf,
      $this->currency,
      '',
    ];
  }

  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->id        = next($fields);
    $this->direction = next($fields);
    $this->adt       = $this->parseFloat(next($fields));
    $this->chd       = $this->parseFloat(next($fields));
    $this->inf       = $this->parseFloat(next($fields));
    $this->currency  = next($fields);

    return $this;
  }
}

/** @} */