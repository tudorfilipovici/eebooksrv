<?php 
namespace App\Edi;

use DateTime;

/**
 * Stores the data from a FLTHEADER record
 */
class FltHeader extends EdifactModel {
  const STATUS_NODIRECT  = 'NODIRECT';
  const STATUS_DIRECT    = 'DIRECT';
  const JOURNEY_OUTBOUND = 1;
  const JOURNEY_INBOUND  = 2;

  protected $status  = '';
  protected $journey = 0;
  protected $date    = null;
  protected $flights = [];

  public function isOutbound() 
  {
    return ($this->journey === self::JOURNEY_OUTBOUND);
  }

  public function isInbound()
  {
    return ($this->journey === self::JOURNEY_INBOUND);
  }

  /**
   * [setTimezone description]
   * @param Req $req [description]
   */
  public function setAirportCode($airportCode) {
    if ($this->date) {
      $this->date = $this->parseAirportDate($airportCode, $this->date->format(DATE_FORMAT_DE), DATE_FORMAT_DE);
      $this->resetTime();
    }
  }

  public function resetTime() {
    if ($this->date instanceof DateTime) {
      if ($this->journey === self::JOURNEY_OUTBOUND) {
        $this->date->setTime(0,0,0);
      } else {
        $this->date->setTime(23,59,59);
      }
    }    
  }

  public function toEdifact()
  {
    return [
      'FLTHEADER',
      $this->journey,
      $this->date->format(DATE_FORMAT_DE),
      $this->status,
      '',
    ];
  }

  /**
   * Adds a set of flight legs wrapped into a Flight object
   * @param array $flightLegs Array of \Edi\FlightLeg objects
   */
  public function addFlightLegs(array $flightLegs) {
    $flight = new Flight($flightLegs);
    $this->flights[$flight->id] = $flight;
    return $flight;
  }

  public function parseEdifact(array $fields)
  {
    $fields = array_pad($fields, 4, '');
    $this->journey = intval($fields[1]);
    $this->date    = $this->parseDate($fields[2], DATE_FORMAT_DE, new DateTime());
    $this->status  = $fields[3];

    // Move date times
    $this->resetTime();
    return $this;
  }
}