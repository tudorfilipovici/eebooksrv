<?php 
namespace App\Edi;

use DateTime;

class Req extends EdifactModel { // implements \JsonSerializable {

  protected $originCode  = '';
  protected $destinCode  = '';
  protected $departure   = null;
  protected $return      = null;
  protected $numPax      = 0;
  protected $numAdt      = 0; // This is a computed value
  protected $numChd      = 0;
  protected $numInf      = 0;

  public function parseEdifact(array $fields) {
    $fields = array_pad($fields, 18, '');
    $this->originCode  = $fields[1];
    $this->destinCode  = $fields[2];
    $depDate           = $fields[3];
    $retDate           = $fields[4];
    $timeType          = $fields[5];
    $depTime           = $fields[6];
    $retTime           = $fields[7];
    $carrCode          = $fields[8];
    $maxStops          = $fields[9];
    $class             = $fields[10];
    $bias              = $fields[11];
    $this->numPax      = $fields[12];
    $this->numChd      = $fields[13];
    $this->numInf      = $fields[14];
    $this->compartment = $fields[15];
    $unifares          = $fields[16];
    $dateType          = $fields[17];
    
    $this->departure = $this->parseAirportDate($this->originCode, $depDate.' '. $depTime, DATETIME_FORMAT_INTERNAL, null);
    $this->departure->setTime(0,0,0);
    if( !empty( $retDate)) {
      $this->return = $this->parseAirportDate($this->destinCode,  $retDate.' '. $retTime, DATETIME_FORMAT_INTERNAL, null);
      $this->return->setTime(23,59,59);
    }
    $this->numAdt = $this->numPax - $this->numChd;
  }

  public function toEdifact() {
    $depDate = $this->departure ? $this->departure->format(DATE_FORMAT_DE) : '';
    $retDate = $this->return    ? $this->return->format(DATE_FORMAT_DE)    : '';
    return ['REQ',
      $this->originCode,
      $this->destinCode,
      $depDate,               // departure date
      $retDate,               // return date
      'D',                    // time type
      $depDate ? '0001' : '', // departure time
      $retDate ? '0001' : '', // return time
      '',                     // carrier
      'N',                    // direct only
      '',                     // class
      '',                     // bias
      $this->numPax,          // num pax
      $this->numChd,          // num chd
      $this->numInf,          // num inf
      '',                     // compartment
      'N',                    // unifares
      'E',                    // date type
      ''                      // endOfRecord
    ];
  }

  /**
   * @return bool
   */
  public function isOneWay() {
    return !($this->return instanceof DateTime);
  }

  /**
   * @return bool
   */
  public function isRoundTrip() {
    return ($this->return instanceof DateTime);
  }

  /**
   * @param string $type
   * @return int
   */
  public function paxCount($type = '') {
    switch($type) {
      case 'adt':
        return $this->numAdt;
      case 'chd':
        return $this->numChd;
      case 'inf';
        return $this->numInf;
    }
    return $this->numPax;
  }

  public function returnDate($format = null) {
    return $format ? $this->return->format($format) : (clone $this->return);
  }

  public function departureDate($format = null) {
    return $format ? $this->departure->format($format) : (clone $this->departure);
  }

  /**
   * Formats dates to string for JSOn serialization
   * This function is used when calling json_encode on a Edi\Req instance
   */
  // function jsonSerialize()
  // {
  //   $data = get_object_vars($this);
  //   if( $this->departure instanceof DateTime) {
  //     $data['departure'] = $this->departure->format(DATETIME_FORMAT_DE);
  //   }
  //   if( $this->return instanceof DateTime) {
  //     $data['return'] = $this->return->format(DATETIME_FORMAT_DE);
  //   }
  //   return $data;
  // }
};
