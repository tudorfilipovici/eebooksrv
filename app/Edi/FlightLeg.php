<?php 
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi;

use DateTime;
use DateTimeZone;
use App\Utils\DateTimeUtil;

/**
 * Stores data from a FLT or CONN record
 */
class FlightLeg extends EdifactModel { //implements \JsonSerializable {
  protected $isConnection  = false;
  protected $fltRef        = '';
  protected $carrierCode   = '';
  protected $carrierName   = '';
  protected $operatorCode  = '';
  protected $operatorName  = '';
  protected $isCodeShare   = false;
  protected $providerName  = '';
  protected $fltNumber     = '';
  protected $originCode    = '';
  protected $destinCode    = '';
  protected $departure     = null;
  protected $arrival       = null;
  protected $numStops      = 0;
  protected $depTerminal   = '';
  protected $arrTerminal   = '';
  protected $equipmentCode = ''; 
  protected $duration      = 0;
  protected $isETicket     = ''; 
  protected $opChangeInd   = ''; 
  protected $classes       = [];

  /**
   * Returns a DateTime instance using the time zone of the given airport and the date/time strings
   * @param  string $airportCode Airport code
   * @param  string $dateStr     Date string
   * @param  string $timeStr     Time string
   * @return \DateTime           
   */
  // protected function getDateTime($airportCode, $dateStr, $timeStr)
  // {
  //   $tzName = DateTimeUtil::getTimeZone($airportCode);
  //   return DateTime::createFromFormat(DATETIME_FORMAT_INTERNAL, $dateStr.' '.$timeStr, new DateTimeZone($tzName));
  // }

  /**
   * Returns the leg duration in mins.
   * The duration takes into account the time zone difference between departure and arrival
   * For example:
   *     Departure FRA 15.06.2016 09:30
   *     Arrival   KIV 15.06.2016 12:50
   *     Duration  140 mins => 02:20 due to 1hr time diff. between FRA and KIV
   * 
   * @param  string $HrsMins
   * @return int
   */
  protected function parseDuration($HrsMins)
  {
    list($hrs, $mins) = str_split($HrsMins, 2);
    return (intval($hrs) * 60) + intval($mins);
  }

  /**
   * [parseFltClass description]
   * @param  array $fields array of one CLASS record
   * @return FltClass      The created instance
   */
  public function parseFltClass($fields)
  {
    $fltClass = FltClass::create($fields);
    $this->classes[] = $fltClass;
    return $fltClass;
  }

  /**
   * @return string Returns the carrier code and flight number
   */
  public function flightNumber()
  {
    return $this->carrierCode.' '.$this->fltNumber;
  }

  /**
   * Returns the duration in Hours and minutes of this leg as string with leading zeros
   * @return array Array with hours and mins strings for duration
   */
  public function durationHrsMins() {
    $hrs  = str_pad(intval($this->duration / 60), 2, '0', STR_PAD_LEFT);
    $mins = str_pad(intval($this->duration % 60), 2, '0', STR_PAD_LEFT);
    return [$hrs,$mins];
  }

  public function departure($format = null) {
    return $format ? $this->departure->format($format) : (clone $this->departure);
  }

  public function arrival($format = null) {
    return $format ? $this->arrival->format($format) : (clone $this->arrival);
  }

  /**
   * Returns a class instance by compartment and booking class
   * @param  array  $searchAttrs  Hash with the attributes to search, for eeMatchIterator
   * @return FltClass             The flight class instance or false if none matches
   */
  // public function findClass(array $searchAttrs) {
  //   return eeFind($this->classes, eeMatchIterator($searchAttrs));
  // }

  /**
   * Override if other transports like Train or Bus are included in the AVAILRES message
   * @return boolean True if this leg is a flight
   */
  public function isAirLeg() {
    return true;
  }

  /**
   * Loads the values from an edifact FLT or CONN array of values
   * @param  array  $fields Array of values
   * @return FlightLeg      Reference to this instance
   */
  public function parseEdifact(array $fields)
  {
    $this->isConnection  = (reset($fields) == 'CONN');
    $this->fltRef        = next($fields);
    $this->carrierCode   = next($fields);
    $this->carrierName   = next($fields);
    $this->operatorCode  = next($fields);
    $this->operatorName  = next($fields);
    $this->isCodeShare   = next($fields) == 'Y';
    $this->providerName  = next($fields);
    $this->fltNumber     = next($fields);
    $this->originCode    = next($fields);
    $this->destinCode    = next($fields);
    $this->departure     = $this->parseAirportDate($this->originCode, next($fields).' '.next($fields), DATETIME_FORMAT_INTERNAL);
    $this->arrival       = $this->parseAirportDate($this->destinCode, next($fields).' '.next($fields), DATETIME_FORMAT_INTERNAL);
    $this->numStops      = intval(next($fields));
    $this->depTerminal   = next($fields);
    $this->arrTerminal   = next($fields);
    $this->equipmentCode = next($fields);
    $this->duration      = $this->parseDuration(next($fields));
    $this->isETicket     = next($fields) == 'Y';
    $this->opChangeInd   = next($fields);
    return $this;
  }

  /**
   * Serializes back to Edifact
   * @return array The CONN or FLT array of values
   */
  public function toEdifact()
  {
    return [
      $this->isConnection ? 'CONN' : 'FLT',
      $this->fltRef,
      $this->carrierCode,
      $this->carrierName,
      $this->operatorCode,
      $this->operatorName,
      $this->isCodeShare ? 'Y' : 'N',
      $this->providerName,
      $this->number,
      $this->originCode,
      $this->destinCode,
      $this->departure->format(DATE_FORMAT_DE),
      $this->departure->format('Hi'),
      $this->arrival->format(DATE_FORMAT_DE),
      $this->arrival->format('Hi'),
      $this->numStops,
      $this->depTerminal,
      $this->arrTerminal,
      $this->equipmentCode,
      implode('', $this->durationHrsMins()),
      $this->isETicket ? 'Y' : 'N',
      $this->opChangeInd,
      ''
    ];
  }

  /**
   * Formats dates to string for JSON serialization
   * This function is used when calling json_encode on a Edi\Req instance
   */
  // function jsonSerialize()
  // {
  //   $data = get_object_vars($this);
  //   $data['departure'] = $departure->format(DATETIME_FORMAT_DE);
  //   $data['arrival']   = $arrival->format(DATETIME_FORMAT_DE);
  //   return $data;
  // }
}
/** @} */