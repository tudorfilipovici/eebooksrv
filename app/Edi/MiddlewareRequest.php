<?php
/**
 * \defgroup MW_REQUEST Edi Requests
 * Edi Requests
 * @{
 */

namespace App\Edi;

abstract class MiddlewareRequest extends EdifactModel
{
  private $errorCode = '';

  public function setErrorCode($err) {
    $this->errorCode = $err;
  }

  public function getErrorCode() 
  {
    return $this->errorCode;
  }  

  protected function sendMessage($name, array $data, $timeout=false)
  {
    // optional if logged in user
    // addUserRecordToMessage($data);

    $msg  = new Message($name, config('client.partner_code'), 10);
    $data = $msg->buildMsg($data);
    $msg->addSegment($data);

    if ($timeout) {
      $msg->timeout = $timeout;
    }

    // send the request, get the response
    $resp   = $msg->sendMsg();
    $resp   = $msg->splitMsg($resp);
    $status = $resp[0][3]; // get the message status

    return [$status, $resp];
  }
}
/** @} */