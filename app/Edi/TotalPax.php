<?php 
/**
 * \defgroup UTIL_PRICE_BREAKDOWN Price Breakdown
 * Common Edifact Records uses for price breakdowns
 * @{
 */

namespace App\Edi;

class TotalPax extends EdifactModel {
  public $id          = '';
  public $paxType     = ''; // Indicates whether this is an adult, child or infant price line
  public $paxNum      = 0;  // Number of passengers of this type that this price applies to
  public $totalFare   = 0;  // Total fare value (fare for a single pax)
  public $totalTaxes  = 0;  // Total taxes value (taxes for a single pax)
  public $totalTSC    = 0;  // Total tsc value (tsc for a single pax)
  public $totalPrice  = 0;  // Total price (fare+taxes+tsc) value for a single pax
  public $paxDiscount = 0;  // Discounted fare value (discounted fare for a single pax)
  public $taxDiscount = 0;  // Discounted taxes value (discounted taxes for a single pax)
  // public $tscDiscount = 0;  // Discounted tsc value (discounted tsc for a single pax)
  public $discount    = 0;  // Total discounted price value (discounted fares+taxes+tsc for a single pax).
  public $currency    = ''; // Currency iso code

  public function toEdifact()
  {
    return [
      'TOTALPAX',
      $this->id,
      $this->paxType,
      $this->paxNum,
      $this->totalFare,
      $this->totalTaxes,
      $this->totalTSC,
      $this->totalPrice,
      $this->paxDiscount,
      $this->taxDiscount,
      // $this->tscDiscount,
      $this->discount,
      $this->currency,
      '',
    ];
  }

  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->id          = next($fields);
    $this->paxType     = next($fields);
    $this->paxNum      = $this->parseInt(next($fields), 0);
    $this->totalFare   = $this->parseFloat(next($fields));
    $this->totalTaxes  = $this->parseFloat(next($fields));
    $this->totalTSC    = $this->parseFloat(next($fields));
    $this->totalPrice  = $this->parseFloat(next($fields));
    $this->paxDiscount = $this->parseFloat(next($fields));
    $this->taxDiscount = $this->parseFloat(next($fields));
    // $this->tscDiscount = $this->parseFloat(next($fields));
    $this->discount    = $this->parseFloat(next($fields));
    $this->currency    = next($fields);
    return $this;
  }
}

/** @} */