<?php

namespace App\Edi;

use Illuminate\Support\Facades\Log;

/**
 * Provides a class to send messages to the backend, EDIFACT based.
 *
 * @file eeMessage.inc
 * @category eeMessage_Functions
 */
class Message
{
  ///@{
  /** @name eeMessage Parts */
  public $type = '';
  public $header = '';
  public $body = '';
  public $footer = '/ENDOFM';
  public $result = '';
  public $status = '';
  public $timeout = 120;
  ///@}

  private $filler = null;
  static public $testCase = null;

  /**
   * Constructor.
   *
   * @param (string) $type       the function to be called
   * @param (string) $lang       language to be used
   * @param (string) $client     client code to be used
   * @param (string) $messageset messageset code for the message to be sent
   * @param (string) $msg        pre-append a message part on initialization
   *
   * ### Globals ###
   * - [in] <b>$pos</b>
   * - [in] <b>$sessionid</b>
   * - [in] <b>$username</b>
   * - [in] <b>$module</b>
   * - [in] <b>$testserver</b>
   *
   * @return void returns nothing
   */
  function __construct( $type, $client, $messageset, $msg = '')
  {
    $lang        = config('client.lang');
    $pos         = config('client.pos');
    $sessionid   = config('client.session_id');
    $username    = config('client.user_id');
    $module      = config('client.module_id');
    $testserver  = config('app.debug');
    $ipAddress   = config('client.ip_address');

    $this->type = $type;

    $this->header = "/" . str_replace('/', '', $username) ."/$type//$lang/$messageset/$client/".
                    substr($sessionid, -20) . "/$pos/" . strtoupper($module) ."/$ipAddress/";

    $this->addSegment($msg);

    if ($testserver) {
      $this->timeout = 240;
    }

    $this->filler = new PatternFiller();
  }

  /**
   * Adds a segment to the body
   *
   * @param (string) $seg The segment to be added
   *
   * @return void returns nothing
   */
  function addSegment($seg)
  {
    if( empty( $seg)) {
      return;
    }

    if( !empty( $this->body)) {
      $this->body .= '|';
    }

    $this->body .= $seg;
  }

  /** @brief Sends the message.
   *
   * ### Globals ###
   * - [out] <b>$ports</b>
   * - [in]  <b>$backend</b>
   * - [in]  <b>$testmessage</b>
   * - [in]  <b>$redirectREQ</b>
   * - [in]  <b>$redirectPORT</b>
   * - [in]  <b>$handbrake</b>
   * - [in]  <b>$abuseUSER</b>
   * - [in]  <b>$REMOTE_ADDR</b>
   * - [in]  <b>$username</b>
   *
   * @return string One of the following:
   * - <b>///TIMEOUT</b>   connection timed out
   * - <b>///HOSTDOWN</b>  host is unreachable
   * - <b>result body</b>  response was received successfully
   */
  function sendMsg()
  {
    $servers      = config('client.booking_servers'); 
    $ports        = config('client.booking_ports'); 
    // $redirectREQ  = config('client.lang');
    // $redirectPORT = config('client.lang');
    // $handbrake    = config('client.lang');
    $testmessage  = config('client.lang'); 
    // $abuseUSER    = config('client.lang'); 
    $ipAddress    = config('client.lang'); 
    // $username     = config('client.lang');

    $type = $this->type;

    // is this user/ip on our list
    // if( isset($abuseUSER[$ipAddress])) {
    //   $abuse = $abuseUSER[$ipAddress];
    // } elseif( isset($abuseUSER[$username])) {
    //   $abuse = $abuseUSER[$username];
    // } else {
    //   $abuse = '';
    // }

    // always random delay and some hostdown...
    // if( $abuse != '') {
    //   sleep(rand(0, $abuse[0])); // random delay

    //   if( $abuse[1] <= rand(0, 10)) {
    //     $this->status = 'HOSTDOWN';
    //     return '///HOSTDOWN';
    //   }
    // }

    // TODO: Implement a test case interface
    if(is_object(self::$testCase) && method_exists(self::$testCase, 'getResponse')) {
      $resp = self::$testCase->getResponse($type);
      if ($resp) {
        // log the request not sent
        // eeGenerateLogEntry(array(
        //   'file' => LOGFILEMSG,
        //   'type' => 'TRACEDBG',
        //   'text' => $this->getMsg(),
        // ));
        Log::debug('TRACEDBG', $this->getMsg());
        return $resp;
      }
    } elseif(isset(self::$testCase[$type])) {
      Log::debug('TRACEDBG', [ self::$testCase[$type] ]);
      return self::$testCase[$type]; // $this->dummyResponse( $type);
    }

    // take the time
    // $start_time = eeSLAStart();

    // set server and port
    // $servers = $backend;

    // is there a redirect on this message
    // if( isset($redirectREQ[$type])) {
    //   $servers = $redirectREQ[$type];

    //   if( isset($redirectPORT[$type])) {
    //     $ports = $redirectPORT[$type];
    //   }
    // }

    // special handling for verify and bookrequest.
    // if( $type == 'VERIFYREQ') {
    //   $this->timeout = 20;
    // } elseif( $handbrake > 0 && !preg_match('#BOOK#', $type)) {
    //   sleep($handbrake);
    // }

    // TODO: Move logic for VERIFYREQ somewhere else
    // $sendcounter = 0;
    // do {
      // send the message
      list($status, $server, $port, $result) = $this->resMessage($servers, $ports);
    //   $sendcounter++;
    // } while( $type == 'VERIFYREQ' && $status != 'SUCCESS' && $sendcounter < 3);

    // stop the time
    // $runtime = eeSLAStop($start_time);

    // eeProcessingLog( "BE~$type~$runtime~$status~$server~$port~$result");
    // Log::info('BE');

    if( $status != 'SUCCESS') {
      // log the request not sent
      // $log['file'] = LOGFILEMSG;
      // $log['type'] = 'TRACENON';
      // $log['text'] = $this->getMsg();
      // eeGenerateLogEntry($log);
      Log::error('TRACENON', [$this->getMsg()]);

      $this->status = $status;

      return "///$status";
    } else {
      $this->status = $result;

      return $this->result;
    }
  }

  /** @brief Sends the request and get the response
   *
   * @param (array) $servers list of all servers to be contacted
   * @param (array) $ports   list of all ports to be used on connect
   *
   * ### Globals ###
   * - [in]   <b>$trace</b>
   * - [in]   <b>$username Not used</b>
   * - [in]   <b>$debug</b>
   *
   * @return (array) array containing 4 elements
   * - 0=process status
   * - 1=server
   * - 2=port
   * - 3=msg status
   */
  function resMessage( $servers, $ports)
  {
    $trace    = config('client.trace');  
    $username = config('client.user_id'); 
    $debug    = config('app.debug');

    $errno  = 0;
    $errstr = '';
    $retry  = 0;

    if( empty($servers) || empty($ports)) {
      return array( 'CONFIG_MISSING', '', '', '');;
    }

    // get random server and port
    $sid  = array_rand($servers); // eeArrayRandomIndex($servers);
    $pid  = array_rand($ports);   // eeArrayRandomIndex($ports);
    $port = $ports[$pid];

    do {
      $server = $servers[$sid];

      // open the socket to the server
      $fp = @fsockopen($server, $port, $errno, $errstr, 20);

      if( !$fp) {
        // log connection problems
        // $log['file'] = LOGFILEERR;
        // $log['type'] = 'TCPERROR';
        // $log['text'] = "Failed to open connection to $server:$port $errstr";
        // eeGenerateLogEntry($log);
        Log::error('TCPERROR', [ "Failed to open connection to $server:$port $errstr" ]);

        // now try the next server
        $sid = ( $sid + 1) % count($servers);
      }
    } while( !$fp && $retry++ < count($servers));

    if( !$fp) {
      if( $errno == 110) {
        return array( 'TIMEOUT', '', '', '');
      }

      return array( 'HOSTDOWN', '', '', '');
    }

    // set the blocking on
    stream_set_blocking($fp, true);
    // but with a timeout
    stream_set_timeout($fp, $this->timeout);

    // build the message
    $msg = $this->getMsg('plain');

    $len = strlen($msg);
    $msg = "$len$msg";

    // If tracing turned on, log it
    if( $trace) {
      // $log['file'] = LOGFILEMSG;
      // $log['type'] = 'TRACEREQ';
      // $log['text'] = $this->getMsg();
      // eeGenerateLogEntry($log);
      Log::notice('TRACEREQ', [ $this->getMsg() ]);
    }

    // write the message header
    @fwrite($fp, "!MSG");
    @fwrite($fp, sprintf('%010d', strlen($msg)));

    // write the message
    @fwrite($fp, $msg);

    // read the response header
    $msghead = @fread($fp, 14);

    if( !$msghead || feof($fp)) {
      fclose($fp);
      return array( 'TIMEOUT', '', '', '');
    }

    // get the header and the messagelength
    $array['head'] = substr($msghead, 0, 4);
    $array['len'] = substr($msghead, 4, 10);

    // read the message
    $msg = '';
    while( !feof($fp)) {
      $msg .= fread($fp, 8192);
    }

    if( !$msg) {
      fclose($fp);
      return array( 'TIMEOUT', '', '', '');
    }

    // ready to close the socket
    fclose($fp);

    // remove CR character
    $this->result = chop($msg);

    // debug for developers
    // if( isset($debug['dumpmsg'])) {
    //   global $dumpmsg;
    //   $dumpmsg[$this->type] = array(
    //     'request'  => explode('|', $this->getMsg()),
    //     'response' => explode('|', $this->getResult()),
    //   );
    // }

    if( strlen($this->result) == 0) {
      // $log['file'] = LOGFILEERR;
      // $log['type'] = 'TCPERROR';
      // $log['text'] = "Timed out waiting for server $server:$port ". $this->getMsg();
      // eeGenerateLogEntry($log);
      Log::error('TCPERROR', ["Timed out waiting for server", $server, $port, $this->getMsg() ]);

      return array( 'TIMEOUT', '', '', '');
    }

    // get the backend status
    if( preg_match("#^[^/]+/[^/]*/[^/]*/([^/]+)/#", $this->result, $regs)) {
      $status = $regs[1];
    } else {
      return array( 'SYNTAX', '', '', 'FORMATERROR');
    }

    // write log if trace is on or wrong result status
    if( $trace) {
      // $log['file'] = LOGFILEMSG;
      // $log['type'] = 'TRACERES';
      // $log['text'] = $this->getResult();
      // eeGenerateLogEntry($log);
      Log::debug('TRACERES', [ $this->getResult() ]);

    } elseif( $status != 'SUCCESS' && $status != 'NONEFOUND') {
      // $log['file'] = LOGFILEMSG;
      // $log['type'] = 'TRACEERR';
      // $log['text'] = $this->getMsg() . " RESULT: " . $this->getResult();
      // eeGenerateLogEntry($log);
      Log::error('TRACEERR', [ $this->getMsg(), $this->getResult() ]);
    }

    return array( 'SUCCESS', $server, $port, $status);
  }

  /**
   * Gets the message header, body and footer.
   *
   * @param (string) $plain The plain message else asterisks for
   * cc, cvc and password
   *
   * @return string the message header, body and footer.
   */
  function getMsg( $plain='')
  {
    $msg = "$this->header|$this->body|$this->footer";

    if( $plain != 'plain') {
      $msg = $this->filler->replace($msg);
    }

    return $msg;
  }

  /**
   * Get the message result
   *
   * @param (string) $plain is plain message else asterisks for cc,
   * cvc and password
   *
   * @return (string) The message result
   */
  function getResult( $plain='')
  {
    $msg = "$this->result";

    if( $plain != 'plain') {
      $msg = $this->filler->replace($msg);
    }

    return $msg;
  }

  /**
   * Split a message to an array of arrays.
   *
   * @param (string) $res_msg The message string
   *
   * @return (array) The splitted message
   */
  function splitMsg( $res_msg)
  {
    $res = array();

    $segment = explode('|', $res_msg);
    $res[] = explode('/', $segment[0]);

    $count = count($segment) - 1;
    for( $i=1; $i < $count; $i++) {
      $res[] = explode('~', $segment[$i]);
    }

    return $res;
  }

  /**
   * Build a message from an array.
   *
   * @param (array) $req_array builds msg from array for sending
   *
   * @return (string) the message
   */
  function buildMsg( $req_array)
  {
    $segment = array();

    $count = count($req_array);
    for( $i = 0; $i < $count; $i++) {
      $segment[] = implode('~', $req_array[$i]);
    }

    return implode('|', $segment);
  }

  // function dummyResponse($type)
  // {
  //   // global $testmessage;

  //   sleep(rand(0, 2)); // emulate load on backend :-)

  //   // eeGenerateLogEntry(array(
  //   //   'file' => LOGFILEDBG,
  //   //   'type' => 'TRACENON',
  //   //   'text' => $this->getMsg(),
  //   // ));
  //   Log::debug('TRACENON', [ $this->getMsg() ]);

  //   return str_replace( array( "\n", "\r", "\r\n", "\n\r"), '', $testmessage[$type]);
  // }
}
?>
