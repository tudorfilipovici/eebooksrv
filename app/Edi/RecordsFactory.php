<?php 
/**
 * \defgroup UTIL_EDIFACT 
 * @{
 */
namespace App\Edi;

use Exception;

/**
 * 
 */
class RecordsFactory {
  protected $records = [];

  public function __construct($classMap) {
    $this->records = $classMap;
  }

  public function register(array $classMap) {
    $this->records = array_merge($this->records, $classMap);
  }

  public function create($type, $values) {
    if (!isset($this->records[$type])) {
      throw new Exception("Records of type '$type' have not been defined", 1);
    }
    $cls = $this->records[$type];
    return new $cls($values);
  }

  public function parse($type, array $values) {
    if (!isset($this->records[$type])) {
      throw new Exception("Records of type '$type' have not been defined", 1);
    }
    $cls = $this->records[$type];
    return $cls::create($values);
  }
}

/** @} */