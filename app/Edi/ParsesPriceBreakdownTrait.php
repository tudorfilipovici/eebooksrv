<?php
namespace App\Edi;

use DateTimeZone;
use DateTime;
use App\Utils\DateTimeUtil;

trait ParsesPriceBreakdownTrait {
  protected $prices    = [];
  protected $taxes     = [];
  protected $totals    = [];

  /**
   * Parse a PRICE record and stores the last instance in $data['PRICE']
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parsePRICE(array $record, array &$data) {
    $price = $this->factory->parse('PRICE', $record);
    $this->prices[$price->id] = $data['PRICE'] = $price;
  }

  /**
   * Parse a TAX record and stores the last instance in $data['TAX']
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parseTAX(array $record, array &$data) {
    $tax = $this->factory->parse('TAX', $record);
    $this->taxes[$tax->id] = $data['TAX'] = $tax;
  }

  /**
   * Parse a TAXELT record and relates it to the last instance in $data['TAX']
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parseTAXELT(array $record, array &$data) {
    $tax = $data['TAX'];
    $tax->addTaxElt($this->factory->parse('TAXELT', $record));
  }

  /**
   * Parse a TOTAL record and stores the last instance in $data['TOTAL']
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parseTOTAL(array $record, array &$data) {
    $total = $this->factory->parse('TOTAL', $record);
    $this->totals[$total->id] = $data['TOTAL'] = $total;
  }

  /**
   * Parse a TOTALPAX record and stores it inside the last instance in $data['TOTAL']
   * @param  array  $record The array of values
   * @param  array  $data   Shared data during the parsing process
   */
  protected function parseTOTALPAX(array $record, array &$data) {
    $total = $data['TOTAL'];
    $total->parseTotalPax($record);
  }
}