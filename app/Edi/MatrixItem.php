<?php
namespace App\Edi;

/**
 * Stores data from a part of a MATRIX record
 */
class MatrixItem extends EdifactModel {
  protected $fltRefOut = '';
  protected $fltRefIn  = '';
  protected $flightOut = null;
  protected $flightIn  = null;
  protected $compOut   = '';
  protected $compIn    = '';
  protected $info      = '';
  protected $fareOut   = null;
  protected $fareIn    = null;
  protected $tax       = null;
  protected $total     = null;
  protected $hasError  = false;

  public function __construct($values) {
    parent::__construct($values);

    // If we have initialization values and the error has not
    // been manually set, check the object data is complete.
    if (!empty($values) && !$this->hasError) {
      $this->hasError = $this->checkError();
    }
  }

  /**
   * Check the object data is complete
   * @return boolean True if data is consistent
   */
  public function checkError() {
    // $errCodes = [
    //   'NOPRICE', 'NOTAX', 'NOFARES', 'NOSEATS', 
    //   'NOSEATSIN', 'NOSEATSOUT', 'CURRENTSEL', 
    //   'NOTALLOWED'/*, 'TOOLATE'*/
    // ];

    $hasErrorCode     = !empty($this->info); // in_array($this->info, $errCodes);
    $outboundNotFound = ($this->fltRefOut != '' && $this->fareOut == null);
    $inboundNotFound  = ($this->fltRefIn  != '' && $this->fareIn  == null);
    $taxNotFound      = ($this->tax   == null);
    $totalNotFound    = ($this->total == null);

    return (
      $hasErrorCode     ||
      $outboundNotFound ||
      $inboundNotFound  ||
      $taxNotFound      ||
      $totalNotFound
    );
  }

  public function getCurrency() {
    return $this->total ? $this->total->currency : null;
  }

  public function getComp($jix) {
    return ($jix == OUTBOUND ? $this->compOut : $this->compIn);
  }

  public function getFlight($jix) {
    return ($jix == OUTBOUND ? $this->flightOut : $this->flightIn);
  }

  public function getFltRef($jix) {
    return ($jix == OUTBOUND ? $this->fltRefOut : $this->fltRefIn);
  }

  public function getFare($jix) {
    return ($jix == OUTBOUND ? $this->fareOut : $this->fareIn);
  }

  public function getTax() {
    return $this->tax;
  }

  public function getTotal() {
    return $this->total;
  }

  /**
   * Function used by de var_dump and Symfony dump functions
   * @return array The debug data to display for this object
   */
  public function __debugInfo() {
    return [
      'fltRefOut' => $this->fltRefOut,
      'fltRefIn'  => $this->fltRefIn,
      'compOut'   => $this->compOut,
      'compIn'    => $this->compIn,
      'info'      => $this->info,
      'fareOut'   => $this->fareOut  ? $this->fareOut->id  : null,
      'fareIn'    => $this->fareIn   ? $this->fareIn->id   : null,
      'tax'       => $this->tax      ? $this->tax->id      : null,
      'total'     => $this->total    ? $this->total->id    : null,
    ];
  }  
}
