<?php 
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi;

/**
 * Stores data from a CLASS record which is related to a FLT or CONN records
 */
class FltClass extends EdifactModel {
  protected $compartment   = '';
  protected $bookingClass  = '';
  protected $numAvailSeats = 0;
  protected $mealCode      = '';

  public function toEdifact()
  {
    return [
      'CLASS',
      $this->compartment,
      $this->bookingClass,
      $this->numAvailSeats,
      $this->mealCode,
      '',
    ];
  }

  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->compartment   = next($fields);
    $this->bookingClass  = next($fields);
    $this->numAvailSeats = intval(next($fields));
    $this->mealCode      = next($fields);
    return $this;
  }
}

/** @} */