<?php
/**
 * \defgroup UTIL_AVAIL UtilAvailability
 * Utility functions for Booking Availability
 * @{
 */

namespace App\Edi;

/**
 * Wraps an array of Edi\FlightLeg
 */
class Flight extends EdifactModel { // implements \JsonSerializable, \IteratorAggregate, \ArrayAccess {
  protected $id   = ''; /**< string, Flight  reference */
  protected $legs = []; /**< string, Flight  legs */

  /**
   * Constructor
   * @param array $fltLegs Array of \Edi\FlightLeg objects
   */
  public function __construct(array $fltLegs = []) {
    $this->legs = $fltLegs;
    $this->id   = $this->firstAirLeg()->fltRef;
  }


  /**
   * Returns true if this is a non stop flight
   * @return boolean [description]
   */
  public function isNonStop()
  {
    return (count($this->legs) == 1);
  }

  /**
   * Get the first leg not part of rail-fly
   * returns the first item of $fltLegs that is an air segment
   *
   * @param array $fltLegs The array of legs
   *
   * @return array
   */
  public function firstAirLeg()
  {
    $firstLeg = reset($this->legs);
    while (!$firstLeg->isAirLeg()) {
        $firstLeg = next($this->legs);
    }
    return $firstLeg;
  }

  /**
   * Get the last leg not part of rail-fly
   * returns the first item of $fltLegs that is an air segment
   *
   * @param array $fltLegs The array of legs
   *
   * @return array
   */
  public function lastAirLeg()
  {
    $lastLeg  = end($this->legs);
    while (!$lastLeg->isAirLeg()) {
        $lastLeg = prev($this->legs);
    }
    return $lastLeg;
  }

  /**
   * returns the total flight duration
   * @return int  Duration in minutes
   */
  public function duration()
  {
    return $this->firstAirLeg()->duration;
  }

  /**
   * Returns true if at least one of the legs in the array have the selected compartment
   * @param  array   $fltLegs     Array of \Edi\FlightLeg objects
   * @param  string  $compartment String
   * @return boolean              [description]
   */
  public function hasCompartment($compartment) 
  {
    foreach ($this->legs as $leg) {
      $cls = $leg->findClass([ 'compartment' => $compartment ]);
      if ($cls) {
        return true;
      }
    }
    return false;
  }

  public function numLegs() {
    return count($this->legs);
  }

  // /**
  //  * Transforms the current instance to a hash array
  //  * @return array The array containing the attributes and values
  //  */
  // public function jsonSerialize() {
  //   $result = get_object_vars($this);
  //   // $result['legs'] = getLegsTemplateData($this->legs);
  //   // $result['destinationCode']    = $this->destinCode;
  //   // $result['destinationName']    = $this->destinName;
  //   // $result['destinationCountry'] = $this->destinCountry;
  //   return $result;
  // }

  // public function getIterator() {
  //   return new \ArrayIterator($this->legs);
  // }

  // public function offsetExists($lix) {
  //   return isset($this->legs[$lix]);
  // }

  // public function offsetGet($lix) {
  //   return isset($this->legs[$lix]) ? $this->legs[$lix] : null;
  // }

  // public function offsetSet($lix, $flightLeg) {
  //   // if (is_null($lix)) {
  //   //   $this->legs[] = $flightLeg;
  //   // } else {
  //   //   $this->legs[$lix] = $flightLeg;
  //   // }    
  //   throw new \ErrorException('Attempted to add/modify legs from a Flight...'); 
  // }

  // public function offsetUnset($lix) {
  //   // unset($this->legs[$lix]);
  //   throw new \ErrorException('Attempted to delete legs from a Flight...'); 
  // }  
}

/** @} End of UTIL_AVAIL */