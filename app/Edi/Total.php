<?php 
/**
 * \defgroup UTIL_PRICE_BREAKDOWN Price Breakdown
 * Common Edifact Records uses for price breakdowns
 * @{
 */

namespace App\Edi;

/**
 * Contains data from a TOTAL segment and the related TOTALPAX details per adt, chd and inf
 */
class Total extends EdifactModel {
  public $id       = '';
  public $amount   = 0;
  public $discount = '';
  public $currency = '';

  public $totalAdt = null;
  public $totalChd = null;
  public $totalInf = null;

  /**
   * Returns a TOTAL array segment
   * @return array TOTAL segment
   */
  public function toEdifact()
  {
    return [
      'TOTAL',
      $this->id,
      $this->amount,
      $this->discount,
      $this->currency,
      '',
    ];
  }

  /**
   * Returns total for the selected pax type
   * @param  string $paxType Pax type: ['ADT', 'CHD', 'INF']
   * @return TotalPax        The total per pax or null if paxType is invalid
   */
  public function getTotalPax($paxType) 
  {
    switch ($paxType) {
      case 'ADT':
        return $this->totalAdt;
      case 'CHD':
        return $this->totalChd;
      case 'INF':
        return $this->totalInf;
    }
    return null;
  }

  /**
   * Parses a PAXTOTAL array segment into a PaxTotal Instance
   * @param  array  $fields PAXTOTAL segment
   * @return TotalPax       The total per pax or null if paxType is invalid
   */
  public function parseTotalPax(array $fields)
  {
    $total = TotalPax::create($fields);
    switch ($total->paxType) {
      case 'ADT':
        $this->totalAdt = $total;
        break;
      case 'CHD':
        $this->totalChd = $total;
        break;
      case 'INF':
        $this->totalInf = $total;
        break;
    }
    return $total;
  }

  /**
   * Parses a TOTAL segment
   * @param  array  $fields TOTAL segment
   * @return Total          Reference to $this
   */
  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->id       = next($fields);
    $this->amount   = $this->parseFloat(next($fields), 0);
    $this->discount = $this->parseFloat(next($fields));
    $this->currency = next($fields);
    return $this;
  }
}
/** @} */