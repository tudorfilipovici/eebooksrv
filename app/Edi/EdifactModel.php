<?php
namespace App\Edi;

use Exception;
use DateTime;
use DateTimeZone;
use App\Utils\DateTimeUtil;

/**
 * Edifact Base model.
 * Provides utility methods and a common interface to parse/serialize Edifact arrays
 */
abstract class EdifactModel {
  /**
   * This is a generic constructor that receives a hash of key/values
   * to be set to the object. For example, if a derived class Person has two properties name and age,
   * you can create a person like this:
   *
   *     $p = new Person([ 'name'=>'John', 'age'=>34 ]);
   *
   * @param array $fields Hash Array with the key/values to parse
   */
  public function __construct(array $fields = []) {
    foreach($fields as $key => $value) {
      $this->$key = $value;
    }
  }

  /**
   * Getter
   * @param  [type] $field [description]
   * @return [type]        [description]
   */
  public function __get($field) 
  {
    if (!isset($this->$field)) {
      throw new Exception("Field '$field' does not exists");
    }

    if (is_object($this->$field)) {
      throw new Exception("Field '$field' is an object, a concrete getter has to be implemented");      
    }

    return $this->$field;
  }

  /**
   * Utility function to create an instance from an Edifact array of values.
   * Creates an instance and calls parseEdifact
   *
   * @param  array  $fields Array of values
   * @return Model         The class instance
   */
  public static function create(array $fields) {
    $model = new static();
    $model->parseEdifact($fields);
    return $model;
  }

  /**
   * Utility function, if the given value is empty returns the default value
   * otherwise it returns the value parsed as float
   *
   * @param  string $val    The value to parse
   * @param  mixed $default The default value in case $val is empty
   * @return mixed          Float or $default
   */
  public static function parseFloat($val, $default = '') {
    return $val == '' ? $default : floatval($val);
  }

  /**
   * Utility function, if the given value is empty returns the default value
   * otherwise it returns the value parsed as int
   *
   * @param  string $val    The value to parse
   * @param  mixed $default The default value in case $val is empty
   * @return mixed          Int or $default
   */
  public static function parseInt($val, $default = '') {
    return $val == '' ? $default : intval($val);
  }

  /**
   * Utility function, if the given value is empty returns the default value
   * otherwise it returns the value parsed as DateTime object
   *
   * @param  [type] $val     The value to parse
   * @param  [type] $fmt     Date format
   * @param  mixed  $default Default value
   * @return mixed           DateTime object or default
   */
  public static function parseDate($val, $fmt, $default = '') {
    return $val == '' ? $default : DateTime::createFromFormat($fmt, $val);
  }


  /**
   * Returns a DateTime instance using the time zone of the given airport and the date/time strings
   * @param  string $airportCode Airport code
   * @param  string $dateStr     Date string
   * @param  string $timeStr     Time string
   * @return \DateTime
   */
  public static function parseAirportDate($airportCode, $dateStr, $format, $default = '')
  {
    try {
      $tzName = DateTimeUtil::getTimeZone($airportCode);
      $tz = new DateTimeZone($tzName);
    } catch(\Exception $err) {
      throw(new \ErrorException("Unknown time zone '$tzName' for airport '$airportCode'"));
    }
    $dt = DateTime::createFromFormat($format, $dateStr, $tz);
    return $dt ?: $default;
  }

  public function toEdifact() {
    throw new \ErrorException('Method toEdifact is not implemented...');
  }

  public function parseEdifact(array $fields) {
    throw new \ErrorException('Method parseEdifact is not implemented...');
  }
};