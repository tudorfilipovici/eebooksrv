<?php 
/**
 * \defgroup UTIL_PRICE_BREAKDOWN Price Breakdown
 * Common Edifact Records uses for price breakdowns
 * @{
 */

namespace App\Edi;

class TaxElement extends EdifactModel {
  use PriceItemTrait;
  public $code = '';

  /**
   * Checks tax element cod to see is is a company tax
   * 
   * @return boolean         True if is a company tax
   */
  public function isCompanyTax() {
    return in_array($this->code, ['YQ','YR']);
  }

  public function toEdifact()
  {
    return [
      'TAXELT',
      $this->code,
      $this->adt,
      $this->chd,
      $this->inf,
      $this->currency,
      '',
    ];
  }

  public function parseEdifact(array $fields)
  {
    reset($fields);
    $this->code     = next($fields);
    $this->adt      = $this->parseFloat(next($fields));
    $this->chd      = $this->parseFloat(next($fields));
    $this->inf      = $this->parseFloat(next($fields));
    $this->currency = next($fields);
    return $this;
  }
}

/** @} */