<?php 
/**
 * \defgroup UTIL_PRICE_BREAKDOWN Price Breakdown
 * Common Edifact Records uses for price breakdowns
 * @{
 */

namespace App\Edi;

trait PriceItemTrait {
  public $adt      = 0;
  public $chd      = 0;
  public $inf      = 0;
  public $currency = '';

  public function total() {
    return ($this->adt + $this->chd + $this->inf);
  }
}

/** @} */