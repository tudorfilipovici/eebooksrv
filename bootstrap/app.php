<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../app/defines.inc';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();
$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Custom Configs
|--------------------------------------------------------------------------
|
| Lumen doesn't load the config/*.php files by default
|
*/
$app->configure('client');
$app->configure('app');


/*
|--------------------------------------------------------------------------
| Custom Logging
|--------------------------------------------------------------------------
|
|
*/
$app->configureMonologUsing(function(Logger $logger) {
    $handlers = [
        ['path' => '/tmp/frontend--debug.log',   'level' => Logger::DEBUG],    // trace
        ['path' => '/tmp/frontend--info.log',    'level' => Logger::INFO],     // avail
        ['path' => '/tmp/frontend--notice.log',  'level' => Logger::NOTICE],   // sla
        ['path' => '/tmp/frontend--warning.log', 'level' => Logger::WARNING],  // missing-text
        ['path' => '/tmp/frontend--error.log',   'level' => Logger::ERROR],        
    ];

    foreach ($handlers as $conf) {
        $handler = new StreamHandler($conf['path'], $conf['level'], false);
        // $handler->setFormatter(new LineFormatter(null, null, true, true));
        $logger->pushHandler($handler);
    }
    return $logger->withName('2e'); // return a clone with a new name
});

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
  App\Http\Middleware\ConfigMiddleware::class,
  App\Http\Middleware\CorsMiddleware::class,
]);

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// $app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);
$app->register(App\Providers\ValidationServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../routes/web.php';
});

return $app;
