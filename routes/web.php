<?php

// Check 
// http://jsonschema.net/
// http://editor.swagger.io/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
  return $app->version();
});

//$app->group(['prefix' => 'flights'], function () use ($app) {
  $app->options('flights',     'FlightBookingController@options');
  $app->get('flights',         'FlightBookingController@search');
  $app->post('flights',        'FlightBookingController@search');
//});
$app->get('routes', 'FlightBookingController@routes');
$app->get('texts',  'TextsController@retrieve');